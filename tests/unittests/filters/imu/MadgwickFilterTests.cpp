#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iostream>

#include <urf/algorithms/filters/imu/MadgwickFilter.hpp>

using urf::algorithms::filters::MadgwickFilter;

TEST(MadgwickFilterShould, getInitialConfiguration) {
    MadgwickFilter filter(100, 0.1);

    ASSERT_NEAR(filter.quaternions()(0), 1.0, 1e-5);
    ASSERT_NEAR(filter.quaternions()(1), 0.0, 1e-5);
    ASSERT_NEAR(filter.quaternions()(2), 0.0, 1e-5);
    ASSERT_NEAR(filter.quaternions()(3), 0.0, 1e-5);

    ASSERT_NEAR(filter.angles()(0), 0.0, 1e-5);
    ASSERT_NEAR(filter.angles()(1), 0.0, 1e-5);
    ASSERT_NEAR(filter.angles()(2), 0.0, 1e-5);
}

TEST(MadgwickFilterShould, updateFilterAnglesFromAccelerations) {
    MadgwickFilter filter(100, 0.5);

    for (int i=0; i < 1000; i++){
        filter.update({0.0, 1.0, 0.0}, {0, 0, 0}, {0,0,0});

    }

    ASSERT_NEAR(filter.angles()(1), 1.57105, 1e-3);
}

