#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "urf/algorithms/compression/VideoCompressionFactory.hpp"
#include "common/compression/jpeg/JpegVideoDecoder.hpp"
#include "common/compression/jpeg/JpegVideoEncoder.hpp"
#include "common/compression/x264/x264VideoDecoder.hpp"
#include "common/compression/x264/x264VideoEncoder.hpp"

using namespace urf::algorithms::compression;

TEST(VideoCompressionFactoryShould, throwExceptionOnUnknownFormat) {
    ASSERT_THROW(VideoCompressionFactory::getDecoder("hello!"), std::invalid_argument);
    ASSERT_THROW(VideoCompressionFactory::getEncoder(
                     "hello!", VideoResolution(1024, 768), 30, CompressionQuality::Fast, true),
                 std::invalid_argument);
}

TEST(VideoCompressionFactoryShould, correctlyGetJpegCompression) {
    auto decoder = VideoCompressionFactory::getDecoder("jpeg");
    ASSERT_NE(reinterpret_cast<JpegVideoDecoder*>(decoder.get()), nullptr);
    auto encoder = VideoCompressionFactory::getEncoder(
        "jpeg", VideoResolution(1024, 768), 30, CompressionQuality::Fast, true);
    ASSERT_NE(reinterpret_cast<JpegVideoEncoder*>(encoder.get()), nullptr);
    ASSERT_EQ(encoder->getName(), "jpeg");
}

TEST(VideoCompressionFactoryShould, correctlyGetx264Compression) {
    auto decoder = VideoCompressionFactory::getDecoder("x264");
    ASSERT_NE(reinterpret_cast<x264VideoDecoder*>(decoder.get()), nullptr);
    auto encoder = VideoCompressionFactory::getEncoder(
        "x264", VideoResolution(1024, 768), 30, CompressionQuality::Fast, true);
    ASSERT_NE(reinterpret_cast<x264VideoEncoder*>(encoder.get()), nullptr);
    ASSERT_EQ(encoder->getName(), "x264");
}
