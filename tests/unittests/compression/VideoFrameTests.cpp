#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <libavutil/frame.h>


#include <urf/algorithms/compression/VideoFrame.hpp>

#include <iostream>
#include <opencv2/core/mat.hpp>

using urf::algorithms::compression::VideoFrame;

TEST(VideoFrameShould, correctlyInitializeFromCvMat) {
    cv::Mat res(480, 640, CV_8UC3);

    auto frame = VideoFrame::from(res);

    ASSERT_EQ(frame.width(), 640);
    ASSERT_EQ(frame.height(), 480);
    ASSERT_EQ(frame.planes(), 1);
    ASSERT_EQ(frame.stride(0), 640*3);
    ASSERT_EQ(frame.pixelFormat(), "bgr24");
}

TEST(VideoFrameShould, correctlyInitializeResizeToCvMat) {
    cv::Mat res(480, 640, CV_8UC3);

    auto frame = VideoFrame::from(res);

    auto retmat = frame.get<cv::Mat>(1280, 720);

    ASSERT_EQ(retmat.rows, 720);
    ASSERT_EQ(retmat.cols, 1280);
}

TEST(VideoFrameShould, correctlyGetAvFrame) {
    cv::Mat res(480, 640, CV_8UC3);

    auto frame = VideoFrame::from(res);
    auto retmat = frame.get<AVFrame*>(1280, 720, "yuv420p");

    ASSERT_EQ(retmat->height, 720);
    ASSERT_EQ(retmat->width, 1280);
}

TEST(VideoFrameShould, correctlyAssignPtrToVideoFrame) {
    cv::Mat res(480, 640, CV_8UC3);

    auto frame = VideoFrame::from(res);
    auto retmat = frame.get<AVFrame*>(1280, 720, "yuv420p");

    VideoFrame backFrame(retmat);
    ASSERT_EQ(backFrame.width(), 1280);
    ASSERT_EQ(backFrame.height(), 720);
    ASSERT_EQ(backFrame.planes(), 3);
    ASSERT_EQ(backFrame.stride(0), 1280);
    ASSERT_EQ(backFrame.stride(1), 1280/2);
    ASSERT_EQ(backFrame.stride(2), 1280/2);
    ASSERT_EQ(backFrame.pixelFormat(), "yuv420p");
}