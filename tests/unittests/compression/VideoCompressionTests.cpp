#include <future>
#include <memory>
#include <vector>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <opencv2/core.hpp>

#include <urf/common/logger/Logger.hpp>

#include <urf/algorithms/compression/IVideoDecoder.hpp>
#include <urf/algorithms/compression/IVideoEncoder.hpp>

#include <urf/algorithms/compression/VideoCompressionFactory.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("VideoCompressionShould");
}

using testing::_;
using testing::AtLeast;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;

using namespace urf::algorithms::compression;

struct VideoCompressionStruct {
    std::string name;
};

class VideoCompressionShould : public ::testing::TestWithParam<VideoCompressionStruct> {
 protected:
    VideoCompressionShould() { }

    void SetUp() override {
        encoder_ = VideoCompressionFactory::getEncoder(
            GetParam().name, VideoResolution(640, 480), 30, CompressionQuality::Normal, false);
        decoder_ = VideoCompressionFactory::getDecoder(GetParam().name);
    }

    void TearDown() override {
        encoder_.reset(nullptr);
        decoder_.reset(nullptr);
    }

    ~VideoCompressionShould() { }

    std::unique_ptr<IVideoEncoder> encoder_;
    std::unique_ptr<IVideoDecoder> decoder_;
};

INSTANTIATE_TEST_SUITE_P(JpegVideoCompression,
                         VideoCompressionShould,
                         ::testing::Values(VideoCompressionStruct{"jpeg"}));

INSTANTIATE_TEST_SUITE_P(x264VideoCompression,
                         VideoCompressionShould,
                         ::testing::Values(VideoCompressionStruct{"x264"}));
#if !defined(_NVJETSON) // We don't execute this test on jetson nano because it takes a lot
INSTANTIATE_TEST_SUITE_P(x265VideoCompression,
                         VideoCompressionShould,
                         ::testing::Values(VideoCompressionStruct{"x265"}));
#endif

INSTANTIATE_TEST_SUITE_P(BmpCompression,
                         VideoCompressionShould,
                         ::testing::Values(VideoCompressionStruct{"bmp"}));

TEST_P(VideoCompressionShould, returnEmptyBufferIfNoImageCompressed) {
    ASSERT_TRUE(encoder_->getBytes().empty());
}

TEST_P(VideoCompressionShould, correctlyEmptyBufferOnGetBytes) {
    VideoResolution resolution = encoder_->getResolution();
    cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);

    for (int i = 0; i < 10; i++) {
        cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
    }

    ASSERT_TRUE(encoder_->flush());

    ASSERT_FALSE(encoder_->getBytes(false).empty());
    ASSERT_FALSE(encoder_->getBytes().empty());
    ASSERT_TRUE(encoder_->getBytes().empty());
}

TEST_P(VideoCompressionShould, cantAddFrameAgainAfterFlush) {
    VideoResolution resolution = encoder_->getResolution();

    cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
    cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));

    ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
    ASSERT_TRUE(encoder_->flush());
    ASSERT_FALSE(encoder_->addFrame(VideoFrame::from(frame)));
}

TEST_P(VideoCompressionShould, correctlyEncodeStreamTest) {
    VideoResolution resolution = encoder_->getResolution();

    for (uint32_t i = 0; i < 30; i++) {
        cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
        cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
    }

    ASSERT_TRUE(encoder_->flush());
    auto buffer = encoder_->getBytes();
    ASSERT_FALSE(buffer.empty());
}

TEST_P(VideoCompressionShould, getQuality) {
    ASSERT_EQ(encoder_->getCompressionQuality(), CompressionQuality::Normal);
}

TEST_P(VideoCompressionShould, returnEmptyFrameIfNotBytesAdded) {
    ASSERT_TRUE(decoder_->getFrame().empty());
}

TEST_P(VideoCompressionShould, correctlyDecodeFrames) {
    VideoResolution resolution = encoder_->getResolution();
    int notEmptyFrames = 0;
    for (int i = 0; i < 100; i++) {
        cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
        cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
        auto bytes = encoder_->getBytes();

        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        if (bytes.empty())
            continue;

        ASSERT_TRUE(decoder_->addBytes(bytes));
        auto decodedFrame = decoder_->getFrame();

        if (!decodedFrame.empty()) {
            ASSERT_EQ(decodedFrame.width(), resolution.width);
            ASSERT_EQ(decodedFrame.height(), resolution.height);
            notEmptyFrames++;
        }
    }

    ASSERT_GT(notEmptyFrames, 50);
}

TEST_P(VideoCompressionShould, DISABLED_doubleEncoderDecoder) {
    VideoResolution resolution2(1280, 720);
    auto encoder2 = VideoCompressionFactory::getEncoder(
        GetParam().name, resolution2, 20, CompressionQuality::Slow, true);
    auto decoder2 = VideoCompressionFactory::getDecoder(GetParam().name);

    VideoResolution resolution = encoder_->getResolution();

    for (int i = 0; i < 100; i++) {
        cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
        cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
        auto bytes = encoder_->getBytes();
        if (!bytes.empty()) {
            ASSERT_TRUE(decoder_->addBytes(bytes));
            auto decodedFrame = decoder_->getFrame();
            if (!decodedFrame.empty()) {
                ASSERT_EQ(decodedFrame.width(), resolution.width);
                ASSERT_EQ(decodedFrame.height(), resolution.height);
            }
        }

        cv::Mat frame2(cv::Size(resolution2.width, resolution2.height), CV_8UC3);
        cv::randu(frame2, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder2->addFrame(VideoFrame::from(frame2)));
        bytes = encoder2->getBytes();
        if (!bytes.empty()) {
            ASSERT_TRUE(decoder2->addBytes(bytes));
            auto decodedFrame = decoder2->getFrame();
            if (!decodedFrame.empty()) {
                ASSERT_EQ(decodedFrame.width(), resolution2.width);
                ASSERT_EQ(decodedFrame.height(), resolution2.height);
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(1000 / 30));
    }
}

TEST_P(VideoCompressionShould, correctlyDecodeFramesIfFirstPacketsAreDropped) {
    VideoResolution resolution = encoder_->getResolution();
    int notEmptyFrames = 0;
    for (int i = 0; i < 10; i++) {
        cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
        cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
        auto bytes = encoder_->getBytes();
    }

    for (int i = 0; i < 300; i++) {
        cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
        cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
        auto bytes = encoder_->getBytes();

        if (bytes.empty())
            continue;
        if (!decoder_->addBytes(bytes))
            continue;

        auto decodedFrame = decoder_->getFrame();

        if (!decodedFrame.empty()) {
            ASSERT_EQ(decodedFrame.width(), resolution.width);
            ASSERT_EQ(decodedFrame.height(), resolution.height);
            notEmptyFrames++;
        }
    }

    ASSERT_GT(notEmptyFrames, 50);
}

TEST_P(VideoCompressionShould, DISABLED_failsToDecodeFrameWithPartialBuffer) {
    VideoResolution resolution = encoder_->getResolution();

    for (int i = 0; i < 10; i++) {
        cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
        cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
        ASSERT_TRUE(encoder_->addFrame(VideoFrame::from(frame)));
        auto bytes = encoder_->getBytes();
        auto length = bytes.size();
        ASSERT_TRUE(
            decoder_->addBytes(std::vector<uint8_t>(bytes.begin(), bytes.begin() + length / 24)));
        auto decodedFrame = decoder_->getFrame();
        ASSERT_TRUE(decodedFrame.empty());
        ASSERT_TRUE(
            decoder_->addBytes(std::vector<uint8_t>(bytes.begin() + length / 24, bytes.end())));
        decodedFrame = decoder_->getFrame();
        ASSERT_FALSE(decodedFrame.empty());
        ASSERT_EQ(decodedFrame.width(), resolution.width);
        ASSERT_EQ(decodedFrame.height(), resolution.height);
    }
}

// TEST_P(VideoCompressionShould, clearTest) {
//     for (int i=0; i < 10; i++) {
//         VideoResolution resolution(100, 100);
//        cv::Mat frame(cv::Size(resolution.width, resolution.height), CV_8UC3);
//         cv::randu(frame, cv::Scalar(0, 0, 0), cv::Scalar(255, 255, 255));
//         ASSERT_TRUE(encoder_->addFrame(frame));
//         auto bytes = encoder_->getBytes();
//         auto length = bytes.size();
//         if (i%2 == 0) {
//             ASSERT_TRUE(decoder_->addBytes(std::vector<uint8_t>(bytes.begin(), bytes.begin() + length/4)));
//             continue;
//         }
//         auto decodedFrame = decoder_->getFrame();
//         ASSERT_TRUE(decodedFrame.empty());
//         ASSERT_TRUE(decoder_->clear());
//         ASSERT_TRUE(decoder_->addBytes(bytes));
//         decodedFrame = decoder_->getFrame();
//         ASSERT_FALSE(decodedFrame.empty());
//         ASSERT_EQ(decodedFrame.width(), resolution.width);
//         ASSERT_EQ(decodedFrame.height(), resolution.height);
//     }
// }
