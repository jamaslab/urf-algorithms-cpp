#include <future>
#include <memory>
#include <vector>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/common/logger/Logger.hpp>

#include <urf/algorithms/compression/IDataDecoder.hpp>
#include <urf/algorithms/compression/IDataEncoder.hpp>

#include <urf/algorithms/compression/lz4/LZ4DataDecoder.hpp>
#include <urf/algorithms/compression/lz4/LZ4DataEncoder.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("DataCompressionShould");
}

using testing::_;
using testing::AtLeast;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;

using namespace urf::algorithms::compression;

struct DataCompressionStruct {
    std::shared_ptr<IDataEncoder> encoder;
    std::shared_ptr<IDataDecoder> decoder;
};

class DataCompressionShould : public ::testing::TestWithParam<DataCompressionStruct> {
 protected:
    DataCompressionShould() { }

    void SetUp() override {
        encoder_ = GetParam().encoder;
        decoder_ = GetParam().decoder;
    }

    void TearDown() override { }

    ~DataCompressionShould() { }

    std::vector<uint8_t> fillRandomVector(uint32_t size) {
        std::vector<uint8_t> retVector;
        retVector.resize(size);
        for (uint32_t i = 0; i < size; i++) {
            retVector[i] = static_cast<uint8_t>(rand() % 256);
        }
        return retVector;
    }

    std::shared_ptr<IDataEncoder> encoder_;
    std::shared_ptr<IDataDecoder> decoder_;
};

INSTANTIATE_TEST_SUITE_P(Lz4DataCompressionNoStream,
                         DataCompressionShould,
                         ::testing::Values(DataCompressionStruct{
                             std::make_shared<LZ4DataEncoder>(false),
                             std::make_shared<LZ4DataDecoder>(false)}));

INSTANTIATE_TEST_SUITE_P(Lz4DataCompressionStream,
                         DataCompressionShould,
                         ::testing::Values(DataCompressionStruct{
                             std::make_shared<LZ4DataEncoder>(true),
                             std::make_shared<LZ4DataDecoder>(true)}));

TEST_P(DataCompressionShould, encodeDecode) {
    auto input = fillRandomVector(1000);
    auto output = encoder_->encode(input);
    ASSERT_FALSE(output.empty());

    LOGGER.debug("Got a compressed vector of size {}", output.size());

    ASSERT_TRUE(decoder_->addBytes(output));

    auto back_vector = decoder_->decode();

    ASSERT_EQ(input.size(), back_vector.size());

    for (size_t i = 0; i < input.size(); i++) {
        ASSERT_EQ(input[i], back_vector[i]);
    }
}

TEST_P(DataCompressionShould, encodeDecodeMultiplePackets) {
    auto input1 = fillRandomVector(1000);
    auto output = encoder_->encode(input1);
    ASSERT_FALSE(output.empty());
    ASSERT_TRUE(decoder_->addBytes(output));
    auto input2 = fillRandomVector(3000);
    output = encoder_->encode(input2);
    ASSERT_FALSE(output.empty());
    ASSERT_TRUE(decoder_->addBytes(output));

    auto back_vector = decoder_->decode();
    ASSERT_EQ(input1.size(), back_vector.size());

    for (size_t i = 0; i < input1.size(); i++) {
        ASSERT_EQ(input1[i], back_vector[i]);
    }

    back_vector = decoder_->decode();
    ASSERT_EQ(input2.size(), back_vector.size());

    for (size_t i = 0; i < input2.size(); i++) {
        ASSERT_EQ(input2[i], back_vector[i]);
    }
}

TEST_P(DataCompressionShould, encodeDecodeBigVector) {
    auto input = fillRandomVector(1e7);
    auto output = encoder_->encode(input);
    ASSERT_FALSE(output.empty());

    LOGGER.debug("Got a compressed vector of size {}", output.size());

    ASSERT_TRUE(decoder_->addBytes(output));

    auto back_vector = decoder_->decode();

    ASSERT_EQ(input.size(), back_vector.size());

    for (size_t i = 0; i < input.size(); i++) {
        ASSERT_EQ(input[i], back_vector[i]);
    }
}

TEST_P(DataCompressionShould, encodeDecodeNoClear) {
    auto input = fillRandomVector(1000);
    auto output = encoder_->encode(input);
    ASSERT_FALSE(output.empty());

    LOGGER.debug("Got a compressed vector of size {}", output.size());

    ASSERT_TRUE(decoder_->addBytes(output));

    auto back_vector = decoder_->decode(false);

    ASSERT_EQ(input.size(), back_vector.size());

    for (size_t i = 0; i < input.size(); i++) {
        ASSERT_EQ(input[i], back_vector[i]);
    }

    back_vector = decoder_->decode(true);
    ASSERT_EQ(input.size(), back_vector.size());

    for (size_t i = 0; i < input.size(); i++) {
        ASSERT_EQ(input[i], back_vector[i]);
    }

    back_vector = decoder_->decode();
    ASSERT_TRUE(back_vector.empty());
}
