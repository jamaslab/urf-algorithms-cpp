#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "urf/algorithms/compression/DataCompressionFactory.hpp"
#include "urf/algorithms/compression/lz4/LZ4DataDecoder.hpp"
#include "urf/algorithms/compression/lz4/LZ4DataEncoder.hpp"

using namespace urf::algorithms::compression;

TEST(DataCompressionFactoryShould, throwExceptionOnUnknownFormat) {
    ASSERT_THROW(DataCompressionFactory::getDecoder("hello!", false), std::invalid_argument);
    ASSERT_THROW(DataCompressionFactory::getEncoder("hello!", false), std::invalid_argument);
}

TEST(DataCompressionFactoryShould, correctlyGetJpegCompression) {
    auto decoder = DataCompressionFactory::getDecoder("lz4", true);
    ASSERT_NE(reinterpret_cast<LZ4DataDecoder*>(decoder.get()), nullptr);
    auto encoder = DataCompressionFactory::getEncoder("lz4", true);
    ASSERT_NE(reinterpret_cast<LZ4DataEncoder*>(encoder.get()), nullptr);
    ASSERT_EQ(encoder->getName(), "lz4");
}
