#include <future>
#include <memory>
#include <vector>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/common/logger/Logger.hpp>

#include <urf/algorithms/compression/pcl/PointCloudDecoder.hpp>
#include <urf/algorithms/compression/pcl/PointCloudEncoder.hpp>

#include <urf/algorithms/compression/lz4/LZ4DataDecoder.hpp>
#include <urf/algorithms/compression/lz4/LZ4DataEncoder.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("PointCloudCompressionShould");
}

using testing::_;
using testing::AtLeast;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;

using namespace urf::algorithms::compression;

struct PointCloudCompressionStruct {
    bool hasOctreeCompression;
    std::shared_ptr<PointCloudEncoder> encoder;
    std::shared_ptr<PointCloudDecoder> decoder;
};

class PointCloudCompressionShould : public ::testing::TestWithParam<PointCloudCompressionStruct> {
 protected:
    PointCloudCompressionShould() { }

    void SetUp() override {
        hasOctreeCompression_ = GetParam().hasOctreeCompression;
        encoder_ = GetParam().encoder;
        decoder_ = GetParam().decoder;
    }

    void TearDown() override { }

    ~PointCloudCompressionShould() { }

    bool hasOctreeCompression_;
    std::shared_ptr<PointCloudEncoder> encoder_;
    std::shared_ptr<PointCloudDecoder> decoder_;

    const size_t testCloudSize_ = 200000;
};

INSTANTIATE_TEST_SUITE_P(WithoutDataCompression,
                         PointCloudCompressionShould,
                         ::testing::Values(PointCloudCompressionStruct{
                             false,
                             std::make_shared<PointCloudEncoder>(false),
                             std::make_shared<PointCloudDecoder>(false)}));

INSTANTIATE_TEST_SUITE_P(WithoutDataCompressionOcTreeCompression,
                         PointCloudCompressionShould,
                         ::testing::Values(PointCloudCompressionStruct{
                             true,
                             std::make_shared<PointCloudEncoder>(true),
                             std::make_shared<PointCloudDecoder>(true)}));

INSTANTIATE_TEST_SUITE_P(
    WithLZ4DataCompression,
    PointCloudCompressionShould,
    ::testing::Values(PointCloudCompressionStruct{
        false,
        std::make_shared<PointCloudEncoder>(false, std::make_unique<LZ4DataEncoder>(true)),
        std::make_shared<PointCloudDecoder>(false, std::make_unique<LZ4DataDecoder>(true))}));

INSTANTIATE_TEST_SUITE_P(
    WithLZ4DataCompressionOcTreeCompression,
    PointCloudCompressionShould,
    ::testing::Values(PointCloudCompressionStruct{
        true,
        std::make_shared<PointCloudEncoder>(true, std::make_unique<LZ4DataEncoder>(true)),
        std::make_shared<PointCloudDecoder>(true, std::make_unique<LZ4DataDecoder>(true))}));

TEST_P(PointCloudCompressionShould, encodeDecodeXYZ) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    for (size_t i = 0; i < testCloudSize_; i++) {
        cloud->push_back(pcl::PointXYZ(i, i, i));
    }

    auto bytes = encoder_->encode<pcl::PointXYZ>(cloud);

    decoder_->addBytes(bytes);

    auto back_cloud = decoder_->decode<pcl::PointXYZ>();

    ASSERT_NE(back_cloud, nullptr);
    ASSERT_EQ(cloud->size(), back_cloud->size());

    if (!hasOctreeCompression_) {
        for (size_t i = 0; i < cloud->size(); i++) {
            ASSERT_EQ(cloud->at(i).x, back_cloud->at(i).x);
            ASSERT_EQ(cloud->at(i).y, back_cloud->at(i).y);
            ASSERT_EQ(cloud->at(i).z, back_cloud->at(i).z);
        }
    }
}

TEST_P(PointCloudCompressionShould, encodeDecodeXYZRGB) {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    for (size_t i = 0; i < testCloudSize_; i++) {
        cloud->push_back(pcl::PointXYZRGB(i, i, i, i % 256, i % 256, i % 256));
    }

    auto bytes = encoder_->encode<pcl::PointXYZRGB>(cloud);

    decoder_->addBytes(bytes);

    auto back_cloud = decoder_->decode<pcl::PointXYZRGB>();

    ASSERT_NE(back_cloud, nullptr);
    ASSERT_EQ(cloud->size(), back_cloud->size());

    if (!hasOctreeCompression_) {
        for (size_t i = 0; i < cloud->size(); i++) {
            ASSERT_EQ(cloud->at(i).x, back_cloud->at(i).x);
            ASSERT_EQ(cloud->at(i).y, back_cloud->at(i).y);
            ASSERT_EQ(cloud->at(i).z, back_cloud->at(i).z);
            ASSERT_EQ(cloud->at(i).r, back_cloud->at(i).r);
            ASSERT_EQ(cloud->at(i).g, back_cloud->at(i).g);
            ASSERT_EQ(cloud->at(i).b, back_cloud->at(i).b);
        }
    }
}

TEST_P(PointCloudCompressionShould, encodeDecodeXYZRGBA) {
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);

    for (size_t i = 0; i < testCloudSize_; i++) {
        cloud->push_back(pcl::PointXYZRGBA(
            i, i, i, i % 256, i % 256, i % 256, i / static_cast<float>(testCloudSize_)));
    }

    auto bytes = encoder_->encode<pcl::PointXYZRGBA>(cloud);

    decoder_->addBytes(bytes);

    auto back_cloud = decoder_->decode<pcl::PointXYZRGBA>();

    ASSERT_NE(back_cloud, nullptr);
    ASSERT_EQ(cloud->size(), back_cloud->size());
    if (!hasOctreeCompression_) {
        for (size_t i = 0; i < cloud->size(); i++) {
            ASSERT_EQ(cloud->at(i).x, back_cloud->at(i).x);
            ASSERT_EQ(cloud->at(i).y, back_cloud->at(i).y);
            ASSERT_EQ(cloud->at(i).z, back_cloud->at(i).z);
            ASSERT_EQ(cloud->at(i).r, back_cloud->at(i).r);
            ASSERT_EQ(cloud->at(i).g, back_cloud->at(i).g);
            ASSERT_EQ(cloud->at(i).b, back_cloud->at(i).b);
            ASSERT_EQ(cloud->at(i).a, back_cloud->at(i).a);
        }
    }
}

TEST_P(PointCloudCompressionShould, cantDecodeDifferentPointType) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    for (size_t i = 0; i < testCloudSize_; i++) {
        cloud->push_back(pcl::PointXYZ(i, i, i));
    }

    auto bytes = encoder_->encode<pcl::PointXYZ>(cloud);

    decoder_->addBytes(bytes);

    auto back_cloud = decoder_->decode<pcl::PointXYZRGB>();

    ASSERT_EQ(back_cloud, nullptr);
}
