#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/algorithms/control/robotbase/RobotBaseOmnidirectionalKinematics.hpp>

using urf::algorithms::control::RobotBaseOmnidirectionalKinematics;

TEST(RobotBaseOmnidirectionalKinematicsShould, throwExceptionIfVectorSizeLessThanFour) {
    RobotBaseOmnidirectionalKinematics kinem(1, 1, 1);

    ASSERT_THROW(kinem.getCartesianVelocity(std::vector<float>{0, 0, 0}), std::invalid_argument);
    ASSERT_THROW(kinem.getCartesianVelocity(std::vector<float>{0, 0, 0, 0, 0}), std::invalid_argument);
}

TEST(RobotBaseOmnidirectionalKinematicsShould, correctlyCalculateBackAndForth) {
    RobotBaseOmnidirectionalKinematics kinem(1, 1, 1);

    auto wheelsVelocity = kinem.getWheelsVelocity(1, 2, 0.2);
    auto cartesianVelocity = kinem.getCartesianVelocity(wheelsVelocity);

    ASSERT_EQ(wheelsVelocity.size(), 4);
    ASSERT_EQ(cartesianVelocity.size(), 3);
    ASSERT_NEAR(cartesianVelocity[0], 1, 1e-5);
    ASSERT_NEAR(cartesianVelocity[1], 2, 1e-5);
    ASSERT_NEAR(cartesianVelocity[2], 0.2, 1e-5);
}
