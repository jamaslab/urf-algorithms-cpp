import os
from conans import ConanFile, CMake, tools


class UrfAlgorithmsCppConan(ConanFile):
    _default_windows_import_paths = [
        "../{cf.settings.os}/{cf.settings.build_type}/bin/{cf.settings.build_type}"]

    name = "urf_algorithms_cpp"
    version = "0.4.0"
    license = "copyright"
    short_paths = True
    author = "Giacomo Lunghi"
    url = "https://github.com/Jamaslab/urf-algorithms-cpp"
    description = "Unified Robotic Framework Algorithms"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "robot_control": [True, False],
        "video_compression": [True, False],
        "data_compression": [True, False],
        "pointcloud_compression": [True, False],
        "filters": [True, False]
    }
    import_paths = []
    default_options = {
        "shared": True,
        "robot_control": True,
        "video_compression": True,
        "data_compression": True,
        "pointcloud_compression": True,
        "filters": True
    }
    requires = ("urf_common_cpp/1.9.0@uji-cirtesu-irslab+urobf+urf-common-cpp/stable", "eigen/3.3.9")
    build_requires = ("gtest/1.10.0", "cmake/3.25.0")
    generators = "cmake", "cmake_find_package", "virtualenv"
    exports_sources = ["environment/*", "src/*", "tests/*",
                       "CMakeLists.txt", "LICENSE", "README.md"]

    @property
    def _is_nvidia_jetson(self):
        if tools.os_info.is_linux:
            try:
                with open('/proc/device-tree/model') as f:
                    jetson_file = f.read()
                    if "jetson" in jetson_file.lower():
                        return True
            except IOError:
                pass
        return False

    @property
    def default_user(self):
        return "uji-cirtesu-irslab+urobf+urf-algorithms-cpp"

    @property
    def default_channel(self):
        return "stable"

    def requirements(self):
        ## Necessary to avoid conflict with ffmpeg openssl versioni
        self.options["cmake"].with_openssl = False

        if self.options.video_compression:
            self.requires("opencv/4.5.0", private=self.options.shared)
            self.requires("libjpeg-turbo/2.1.4", private=self.options.shared)
            self.requires("zlib/1.2.13", private=self.options.shared)

            if self._is_nvidia_jetson:
                self.requires("ffmpeg/4.2.7@uji-cirtesu-irslab+urobf+urf-externals/stable", private=self.options.shared)
            else:
                self.requires("ffmpeg/5.0", private=self.options.shared)

        if self.options.pointcloud_compression:
            self.requires("pcl/1.11.1@uji-cirtesu-irslab+urobf+urf-externals/stable")
            self.requires("lz4/1.9.3")
            self.requires("flann/1.9.2")
            self.requires("eigen/3.3.9")

        if self.options.data_compression:
            self.requires("lz4/1.9.3")

        if self.options.video_compression and self.options.pointcloud_compression:
            self.requires("libpng/1.6.39")

    def configure(self):
        self.options["urf_common_cpp"].shared = False
        if self.options.video_compression:
            self.options["opencv"].with_jpeg = "libjpeg-turbo"
            self.options["opencv"].with_ffmpeg = False
            self.options["opencv"].with_webp = False
            self.options["opencv"].with_ade = False

            if self._is_nvidia_jetson:
                self.options["opencv"].with_jpeg = False
                self.options["opencv"].with_tiff = False
                self.options["opencv"].with_jpeg2000 = False

            if not self._is_nvidia_jetson:
                self.options["ffmpeg"].postproc = False
                self.options["ffmpeg"].with_bzip2 = False
                self.options["ffmpeg"].with_zlib = False
                self.options["ffmpeg"].with_lzma = False
                self.options["ffmpeg"].with_libiconv = False
                self.options["ffmpeg"].with_freetype = False
                self.options["ffmpeg"].with_openh264 = False
                self.options["ffmpeg"].with_zeromq = False
                self.options["ffmpeg"].with_sdl = False

                if self.settings.os in ["Linux", "FreeBSD"]:
                    self.options["ffmpeg"].with_xcb = False
                    self.options["opencv"].with_gtk = False
                    self.options["opencv"].with_v4l = False

                if self.settings.os in ["Macos", "iOS", "tvOS"]:
                    self.options["ffmpeg"].with_appkit = False
                    self.options["ffmpeg"].with_coreimage = False
                    self.options["ffmpeg"].with_audiotoolbox = False
                    self.options["ffmpeg"].with_videotoolbox = False
                self.options["ffmpeg"].with_programs = True

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(
            self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace(
            "\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = not self.options.shared

        cmake.definitions["VIDEO_COMPRESSION"] = self.options.video_compression
        cmake.definitions["DATA_COMPRESSION"] = self.options.data_compression
        cmake.definitions["POINTCLOUD_COMPRESSION"] = self.options.pointcloud_compression
        cmake.definitions["ROBOT_CONTROL"] = self.options.robot_control
        cmake.definitions["FILTERS"] = self.options.filters

        cmake.configure(
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.test(output_on_failure=True)
        cmake.install()

    def imports(self):
        if tools.os_info.is_windows:
            import_paths = getattr(self, 'import_paths') + \
                self._default_windows_import_paths
            for ipath in import_paths:
                self.copy("*.dll", str(ipath.format(cf=self)), "bin")
                self.copy("*.dll", str(ipath.format(cf=self)), "lib")
                self.copy("*.dylib", str(ipath.format(cf=self)), "lib")

    def package(self):
        self.copy("*.hpp", dst=".", src=os.path.join(self.recipe_folder,
                  'package/'), keep_path=True)
        self.copy("*.h", dst=".", src=os.path.join(self.recipe_folder,
                  'package/'), keep_path=True)
        if tools.os_info.is_linux:
            self.copy(
                "*.so", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy(
                "*.a", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
        elif tools.os_info.is_windows:
            self.copy(
                "*.dll", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy(
                "*.lib", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)

    def package_info(self):
        self.cpp_info.libs = ['urf_algorithms']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']
