#include "urf/algorithms/filters/imu/MadgwickFilter.hpp"

namespace {
static float invSqrt(float x) {
    union {
        float f;
        uint32_t i;
    } conv;

    float x2;
    const float threehalfs = 1.5F;

    x2 = x * 0.5F;
    conv.f = x;
    conv.i = 0x5f3759df - (conv.i >> 1);
    conv.f = conv.f * (threehalfs - (x2 * conv.f * conv.f));
    return conv.f;
}

} // namespace

namespace urf {
namespace algorithms {
namespace filters {

MadgwickFilter::MadgwickFilter(float frequency, float beta)
    : _beta(beta)
    , _q()
    , _freq(frequency)
    , _invFreq(1.0f / frequency) {
    reset();
}

bool MadgwickFilter::reset() {
    _q(0) = 1;
    _q(1) = 0;
    _q(2) = 0;
    _q(3) = 0;

    return true;
}

bool MadgwickFilter::update(const Eigen::Vector3f& accelerations,
                            const Eigen::Vector3f& gyroscopes) {
    float ax = accelerations(0);
    float ay = accelerations(1);
    float az = accelerations(2);

    float gx = gyroscopes(0);
    float gy = gyroscopes(1);
    float gz = gyroscopes(2);

    float recipNorm;

    Eigen::Vector4f qDot;
    Eigen::Vector4f s;
    float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2, _8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

    // No need to check filter pointer -- it's checked by _update()

    // Rate of change of quaternion from gyroscope
    qDot(0) = 0.5f * (-_q(1) * gx - _q(2) * gy - _q(3) * gz);
    qDot(1) = 0.5f * (_q(0) * gx + _q(2) * gz - _q(3) * gy);
    qDot(2) = 0.5f * (_q(0) * gy - _q(1) * gz + _q(3) * gx);
    qDot(3) = 0.5f * (_q(0) * gz + _q(1) * gy - _q(2) * gx);
    // Normalise accelerometer measurement
    recipNorm = invSqrt(ax * ax + ay * ay + az * az);
    ax *= recipNorm;
    ay *= recipNorm;
    az *= recipNorm;

    // Auxiliary variables to avoid repeated arithmetic
    _2q0 = 2.0f * _q(0);
    _2q1 = 2.0f * _q(1);
    _2q2 = 2.0f * _q(2);
    _2q3 = 2.0f * _q(3);
    _4q0 = 4.0f * _q(0);
    _4q1 = 4.0f * _q(1);
    _4q2 = 4.0f * _q(2);
    _8q1 = 8.0f * _q(1);
    _8q2 = 8.0f * _q(2);
    q0q0 = _q(0) * _q(0);
    q1q1 = _q(1) * _q(1);
    q2q2 = _q(2) * _q(2);
    q3q3 = _q(3) * _q(3);

    // Gradient decent algorithm corrective step
    s(0) = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
    s(1) = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * _q(1) - _2q0 * ay - _4q1 + _8q1 * q1q1 +
         _8q1 * q2q2 + _4q1 * az;
    s(2) = 4.0f * q0q0 * _q(2) + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 +
         _8q2 * q2q2 + _4q2 * az;
    s(3) = 4.0f * q1q1 * _q(3) - _2q1 * ax + 4.0f * q2q2 * _q(3) - _2q2 * ay;
    recipNorm = invSqrt(s(0) * s(0) + s(1) * s(1) + s(2) * s(2) + s(3) * s(3));
    
    // normalise step magnitude
    s *= recipNorm;

    // Apply feedback step
    qDot -= s*_beta;

    // Integrate rate of change of quaternion to yield quaternion
    _q += qDot*_invFreq;

    // Normalise quaternion
    recipNorm = invSqrt(_q(0) * _q(0) + _q(1) * _q(1) + _q(2) * _q(2) + _q(3) * _q(3));
    _q *= recipNorm;

    return true;
}

bool MadgwickFilter::update(const Eigen::Vector3f& accelerations,
                            const Eigen::Vector3f& gyroscopes,
                            const Eigen::Vector3f& magnetometers) {
    float recipNorm;
    Eigen::Vector4f s;
    Eigen::Vector4f qDot;
    float hx, hy;
    float _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2,
        _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;

    float ax = accelerations(0);
    float ay = accelerations(1);
    float az = accelerations(2);

    float gx = gyroscopes(0);
    float gy = gyroscopes(1);
    float gz = gyroscopes(2);

    float mx = magnetometers(0);
    float my = magnetometers(1);
    float mz = magnetometers(2);

    // Rate of change of quaternion from gyroscope
    qDot(0) = 0.5f * (-_q(1) * gx - _q(2) * gy - _q(3) * gz);
    qDot(1) = 0.5f * (_q(0) * gx + _q(2) * gz - _q(3) * gy);
    qDot(2) = 0.5f * (_q(0) * gy - _q(1) * gz + _q(3) * gx);
    qDot(3) = 0.5f * (_q(0) * gz + _q(1) * gy - _q(2) * gx);

    // Normalise accelerometer measurement
    recipNorm = invSqrt(ax * ax + ay * ay + az * az);
    ax *= recipNorm;
    ay *= recipNorm;
    az *= recipNorm;

    // Normalise magnetometer measurement
    recipNorm = invSqrt(mx * mx + my * my + mz * mz);
    mx *= recipNorm;
    my *= recipNorm;
    mz *= recipNorm;

    // Auxiliary variables to avoid repeated arithmetic
    _2q0mx = 2.0f * _q(0) * mx;
    _2q0my = 2.0f * _q(0) * my;
    _2q0mz = 2.0f * _q(0) * mz;
    _2q1mx = 2.0f * _q(1) * mx;
    _2q0 = 2.0f * _q(0);
    _2q1 = 2.0f * _q(1);
    _2q2 = 2.0f * _q(2);
    _2q3 = 2.0f * _q(3);
    _2q0q2 = 2.0f * _q(0) * _q(2);
    _2q2q3 = 2.0f * _q(2) * _q(3);
    q0q0 = _q(0) * _q(0);
    q0q1 = _q(0) * _q(1);
    q0q2 = _q(0) * _q(2);
    q0q3 = _q(0) * _q(3);
    q1q1 = _q(1) * _q(1);
    q1q2 = _q(1) * _q(2);
    q1q3 = _q(1) * _q(3);
    q2q2 = _q(2) * _q(2);
    q2q3 = _q(2) * _q(3);
    q3q3 = _q(3) * _q(3);

    // Reference direction of Earth's magnetic field
    hx = mx * q0q0 - _2q0my * _q(3) + _2q0mz * _q(2) + mx * q1q1 + _2q1 * my * _q(2) +
         _2q1 * mz * _q(3) - mx * q2q2 - mx * q3q3;
    hy = _2q0mx * _q(3) + my * q0q0 - _2q0mz * _q(1) + _2q1mx * _q(2) - my * q1q1 + my * q2q2 +
         _2q2 * mz * _q(3) - my * q3q3;
    _2bx = sqrtf(hx * hx + hy * hy);
    _2bz = -_2q0mx * _q(2) + _2q0my * _q(1) + mz * q0q0 + _2q1mx * _q(3) - mz * q1q1 +
           _2q2 * my * _q(3) - mz * q2q2 + mz * q3q3;
    _4bx = 2.0f * _2bx;
    _4bz = 2.0f * _2bz;

    // Gradient decent algorithm corrective step
    s(0) = -_2q2 * (2.0f * q1q3 - _2q0q2 - ax) + _2q1 * (2.0f * q0q1 + _2q2q3 - ay) -
         _2bz * _q(2) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) +
         (-_2bx * _q(3) + _2bz * _q(1)) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) +
         _2bx * _q(2) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s(1) = _2q3 * (2.0f * q1q3 - _2q0q2 - ax) + _2q0 * (2.0f * q0q1 + _2q2q3 - ay) -
         4.0f * _q(1) * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) +
         _2bz * _q(3) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) +
         (_2bx * _q(2) + _2bz * _q(0)) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) +
         (_2bx * _q(3) - _4bz * _q(1)) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s(2) =
        -_2q0 * (2.0f * q1q3 - _2q0q2 - ax) + _2q3 * (2.0f * q0q1 + _2q2q3 - ay) -
        4.0f * _q(2) * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) +
        (-_4bx * _q(2) - _2bz * _q(0)) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) +
        (_2bx * _q(1) + _2bz * _q(3)) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) +
        (_2bx * _q(0) - _4bz * _q(2)) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s(3) =
        _2q1 * (2.0f * q1q3 - _2q0q2 - ax) + _2q2 * (2.0f * q0q1 + _2q2q3 - ay) +
        (-_4bx * _q(3) + _2bz * _q(1)) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) +
        (-_2bx * _q(0) + _2bz * _q(2)) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) +
        _2bx * _q(1) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    recipNorm = invSqrt(s(0) * s(0) + s(1) * s(1) + s(2) * s(2) + s(3) * s(3));
    // normalise step magnitude
    s *= recipNorm;

    // Apply feedback step
    qDot -= s*_beta;

    // Integrate rate of change of quaternion to yield quaternion
    _q += qDot*_invFreq;

    // Normalise quaternion
    recipNorm = invSqrt(_q(0) * _q(0) + _q(1) * _q(1) + _q(2) * _q(2) + _q(3) * _q(3));
    _q *= recipNorm;

    return true;
}

Eigen::Vector4f MadgwickFilter::quaternions() const {
    return _q;
}

Eigen::Vector3f MadgwickFilter::angles() const {
    Eigen::Vector3f retval;
    retval(0) = asinf(-2.0f * (_q(1) * _q(3) - _q(0) * _q(2)));
    retval(1) = atan2f(_q(0) * _q(1) + _q(2) * _q(3), 0.5f - _q(1) * _q(1) - _q(2) * _q(2));
    retval(2) = atan2f(_q(1) * _q(2) + _q(0) * _q(3), 0.5f - _q(2) * _q(2) - _q(3) * _q(3));
    return retval;
}

} // namespace filters
} // namespace algorithms
} // namespace urf