#include "urf/algorithms/control/robotbase/RobotBaseOmnidirectionalKinematics.hpp"

#include <stdexcept>

namespace urf {
namespace algorithms {
namespace control {

RobotBaseOmnidirectionalKinematics::RobotBaseOmnidirectionalKinematics(float wheelRadius,
                                                                       float wheelCenterX,
                                                                       float wheelCenterY)
    : wheelRadius_(wheelRadius)
    , wheelCenterX_(wheelCenterX)
    , wheelCenterY_(wheelCenterY) { }

std::vector<float>
RobotBaseOmnidirectionalKinematics::getWheelsVelocity(float v_x, float v_y, float w) const {
    float positionSum = wheelCenterX_ + wheelCenterY_;
    return std::vector<float>{(v_x - v_y - positionSum * w) / wheelRadius_,
                              (v_x + v_y + positionSum * w) / wheelRadius_,
                              (v_x + v_y - positionSum * w) / wheelRadius_,
                              (v_x - v_y + positionSum * w) / wheelRadius_};
}

std::vector<float> RobotBaseOmnidirectionalKinematics::getCartesianVelocity(
    const std::vector<float>& wheelsVelocity) const {
    if (wheelsVelocity.size() != 4) {
        throw std::invalid_argument("Input vector must have size 4");
    }

    float positionSum = wheelCenterX_ + wheelCenterY_;
    return std::vector<float>{
        (wheelsVelocity[0] + wheelsVelocity[1] + wheelsVelocity[2] + wheelsVelocity[3])*wheelRadius_/4,
        (-wheelsVelocity[0] + wheelsVelocity[1] + wheelsVelocity[2] - wheelsVelocity[3])*wheelRadius_/4,
        (-wheelsVelocity[0] + wheelsVelocity[1] - wheelsVelocity[2] + wheelsVelocity[3])*wheelRadius_/(4*positionSum)
    };
}

} // namespace control
} // namespace algorithms
} // namespace urf