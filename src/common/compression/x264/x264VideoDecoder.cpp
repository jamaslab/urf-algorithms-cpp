#include "x264VideoDecoder.hpp"

#include <algorithm>
#include <opencv2/imgproc.hpp>

#include <urf/common/logger/Logger.hpp>

#ifdef av_err2str
#    undef av_err2str
#    include <string>
av_always_inline std::string av_err2string(int errnum) {
    char str[AV_ERROR_MAX_STRING_SIZE];
    return av_make_error_string(str, AV_ERROR_MAX_STRING_SIZE, errnum);
}
#    define av_err2str(err) av_err2string(err).c_str()
#endif // av_err2str

namespace {
auto LOGGER = urf::common::getLoggerInstance("x264VideoDecoder");
}

namespace urf {
namespace algorithms {
namespace compression {

x264VideoDecoder::x264VideoDecoder()
    : buffer_()
    , codec_(nullptr)
    , context_(nullptr)
    , packet_(nullptr)
    , selectedDevType_(AVHWDeviceType::AV_HWDEVICE_TYPE_NONE) {
    LOGGER.trace("CTor");

    packet_ = av_packet_alloc();
    av_log_set_level(AV_LOG_QUIET);
#if defined(_NVJETSON)
    codec_ = avcodec_find_decoder_by_name("h264_nvv4l2dec");
#else
    codec_ = avcodec_find_decoder(AV_CODEC_ID_H264);
#endif
    if (!codec_) {
        LOGGER.warn("avcodec_find_encoder failed");
        throw std::runtime_error("avcodec_find_encoder failed");
    }

    // Iterate over available hw devices to check if there's one available for the specified codec
    AVHWDeviceType hwDevType = AVHWDeviceType::AV_HWDEVICE_TYPE_NONE;
    while (((hwDevType = av_hwdevice_iterate_types(hwDevType)) !=
            AVHWDeviceType::AV_HWDEVICE_TYPE_NONE) &&
           (selectedDevType_ == AVHWDeviceType::AV_HWDEVICE_TYPE_NONE)) {
        LOGGER.info("Supported hardware device name: {}", av_hwdevice_get_type_name(hwDevType));
        for (int i = 0; i < 256; i++) {
            const AVCodecHWConfig* config = avcodec_get_hw_config(codec_, i);
            if (!config) {
                continue;
            }

            if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX &&
                config->device_type == hwDevType) {
                LOGGER.info("Using hardware device {}", av_hwdevice_get_type_name(hwDevType));
                selectedDevType_ = hwDevType;
                break;
            }
        }
    }

    hwDevType = AVHWDeviceType::AV_HWDEVICE_TYPE_NONE;
    context_ = avcodec_alloc_context3(codec_);
    if (!context_) {
        LOGGER.warn("avcodec_alloc_context3 failed");
        throw std::runtime_error("avcodec_alloc_context3 failed");
    }

    if (selectedDevType_ != AVHWDeviceType::AV_HWDEVICE_TYPE_NONE) {
        auto retval = av_hwdevice_ctx_create(&hwDeviceCtx_, selectedDevType_, NULL, NULL, 0);
        if (retval == 0) {
            context_->hw_device_ctx = av_buffer_ref(hwDeviceCtx_);
            hwFrame_ = av_frame_alloc();
        } else {
            selectedDevType_ = AVHWDeviceType::AV_HWDEVICE_TYPE_NONE;
            LOGGER.warn("Failed to av_hwdevice_ctx_create. Using software encoding: {} {}",
                        retval,
                        av_err2str(retval));
        }
    }

    context_->codec_type = AVMEDIA_TYPE_VIDEO;
    if (avcodec_open2(context_, codec_, NULL) < 0) {
        LOGGER.warn("avcodec_open2 failed");
        throw std::runtime_error("avcodec_open2 failed");
    }
}

x264VideoDecoder::~x264VideoDecoder() {
    avcodec_close(context_);
    av_free(context_);

    if (hwDeviceCtx_) {
        av_buffer_unref(&hwDeviceCtx_);
        av_frame_free(&hwFrame_);
    }
}

bool x264VideoDecoder::addBytes(const std::vector<uint8_t>& bytes) {
    if (bytes.empty())
        return true;

    auto localCopy = bytes;
    packet_->size = static_cast<int>(localCopy.size());
    packet_->data = localCopy.data();

    auto ret_val = avcodec_send_packet(context_, packet_);
    if (ret_val < 0) {
        LOGGER.warn("avcodec_send_packet failed {}", av_err2str(ret_val));
        return false;
    }

    return true;
}

VideoFrame x264VideoDecoder::getFrame() {
    if (selectedDevType_ == AVHWDeviceType::AV_HWDEVICE_TYPE_NONE) {
        auto frame = av_frame_alloc();
        avcodec_receive_frame(context_, frame);
        VideoFrame res(frame);
        av_frame_free(&frame);
        return res;
    } else {
        int got_picture = avcodec_receive_frame(context_, hwFrame_);
        if (got_picture != 0) {
            LOGGER.warn("Did not get picture: {}", av_err2str(got_picture));
            return VideoFrame{};
        }

        auto frame = av_frame_alloc();
        if (av_hwframe_transfer_data(frame, hwFrame_, 0) < 0) {
            LOGGER.error("Failed to transfer data from hardware");
            return VideoFrame{};
        }

        VideoFrame res(frame);
        av_frame_free(&frame);
        return res;
    }
}

bool x264VideoDecoder::clear() {
    LOGGER.trace("clear()");
    return true;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
