#include "x264VideoEncoder.hpp"

#include <opencv2/imgproc.hpp>
#include <x264.h>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("x264VideoEncoder");
}

namespace urf {
namespace algorithms {
namespace compression {

x264VideoEncoder::x264VideoEncoder(VideoResolution resolution,
                                   uint16_t fps,
                                   CompressionQuality quality,
                                   bool zeroLatency)
    : resolution_(resolution)
    , quality_(quality)
    , zeroLatency_(zeroLatency)
    , codec_(nullptr)
    , context_(nullptr)
    , selectedDevType_(AVHWDeviceType::AV_HWDEVICE_TYPE_NONE)
    , flushed_(false)
    , flowBytes_(false) {
    LOGGER.trace("CTor");
    av_log_set_level(AV_LOG_QUIET);

    codec_ = avcodec_find_encoder(AV_CODEC_ID_H264);
    if (!codec_) {
        LOGGER.warn("avcodec_find_encoder failed");
        throw std::runtime_error("avcodec_find_encoder failed");
    }

    // Iterate over available hw devices to check if there's one available for the specified codec
    AVHWDeviceType hwDevType = AVHWDeviceType::AV_HWDEVICE_TYPE_NONE;
    while (((hwDevType = av_hwdevice_iterate_types(hwDevType)) !=
            AVHWDeviceType::AV_HWDEVICE_TYPE_NONE) &&
           (selectedDevType_ == AVHWDeviceType::AV_HWDEVICE_TYPE_NONE)) {
        LOGGER.info("Supported hardware device name: {}", av_hwdevice_get_type_name(hwDevType));
        for (int i = 0; i < 256; i++) {
            const AVCodecHWConfig* config = avcodec_get_hw_config(codec_, i);
            if (!config) {
                continue;
            }

            if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX &&
                config->device_type == hwDevType) {
                LOGGER.info("Using hardware device {}", av_hwdevice_get_type_name(hwDevType));
                selectedDevType_ = hwDevType;
                break;
            }
        }
    }

    context_ = avcodec_alloc_context3(codec_);
    if (!context_) {
        LOGGER.warn("avcodec_alloc_context3 failed");
        throw std::runtime_error("avcodec_alloc_context3 failed");
    }

    //context_->bit_rate = h264_config.bit_rate;//The picture is blurred
    int compressionFactor = static_cast<int>(51 - ((static_cast<int>(quality) * 1.25) * 5.1));
    context_->width = resolution.width;
    context_->height = resolution.height;
    context_->time_base = {1, fps};
    context_->framerate = {fps, 1};
    context_->gop_size = 250;
    context_->max_b_frames = 0;
    context_->pix_fmt = AV_PIX_FMT_YUV420P;
    context_->codec_id = AV_CODEC_ID_H264;
    context_->codec_type = AVMEDIA_TYPE_VIDEO;
    context_->qmin = compressionFactor;
    context_->qmax = compressionFactor;

    AVDictionary* dict = nullptr;
    av_dict_set(&dict, "preset", x264_preset_names[static_cast<int>(quality_)], 1);
    av_dict_set(&dict, "tune", "zerolatency", zeroLatency);
    av_dict_set(&dict, "profile", "main", 0);

    packet_ = av_packet_alloc();
    if (!packet_) {
        LOGGER.warn("Failed to allocate frames and packet");
        return;
    }

    if (avcodec_open2(context_, codec_, &dict) < 0) {
        LOGGER.warn("Failed to open codec");
        return;
    }
}

x264VideoEncoder::~x264VideoEncoder() {
    if (context_)
        avcodec_free_context(&context_);

    if (packet_)
        av_packet_free(&packet_);
}

bool x264VideoEncoder::addFrame(const VideoFrame& frame) {
    if (flushed_) {
        LOGGER.warn("Already flushed");
        return false;
    }

    auto avFrame = frame.get<VideoFrame>(resolution_.width, resolution_.height, "yuv420p");
    int r = avcodec_send_frame(context_, static_cast<const AVFrame*>(avFrame.ptr()));
    if (r >= 0) {
        r = avcodec_receive_packet(context_, packet_);
        if (r == 0) {
            flowBytes_.insert(flowBytes_.end(),
                              reinterpret_cast<uint8_t*>(packet_->data),
                              reinterpret_cast<uint8_t*>(packet_->data) + packet_->size);
            return true;
        }

        if (r == AVERROR(EAGAIN) || r == AVERROR_EOF) {
            return false;
        }
    }
    return false;
}

std::vector<uint8_t> x264VideoEncoder::getBytes(bool clearBuffer) {
    if (clearBuffer) {
        auto copy = flowBytes_;
        flowBytes_.clear();
        return copy;
    }

    return flowBytes_;
}

bool x264VideoEncoder::flush() {
    LOGGER.trace("flush()");
    if (zeroLatency_) {
        LOGGER.warn("Can't flush in zerolatency mode");
        return false;
    }

    int ret = -1;
    do {
        ret = avcodec_receive_packet(context_, packet_);
        if (ret == 0) {
            flowBytes_.insert(flowBytes_.end(),
                              reinterpret_cast<uint8_t*>(packet_->data),
                              reinterpret_cast<uint8_t*>(packet_->data) + packet_->size);
        }
    } while ((ret != AVERROR(EAGAIN)) && (ret != AVERROR_EOF));
    flushed_ = true;
    return true;
}

CompressionQuality x264VideoEncoder::getCompressionQuality() {
    LOGGER.trace("getCompressionQuality");
    return quality_;
}

VideoResolution x264VideoEncoder::getResolution() {
    LOGGER.trace("getResolution");
    return resolution_;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
