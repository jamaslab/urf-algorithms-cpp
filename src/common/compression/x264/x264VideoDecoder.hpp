#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/algorithms/urf_algorithms_export.h"
#else
#    define URF_ALGORITHMS_EXPORT
#endif

#    ifdef __cplusplus
extern "C" {
#    endif
#    include "libavcodec/avcodec.h"
#    include <libswscale/swscale.h>
#    ifdef __cplusplus
};
#    endif

#include <string>

#include "urf/algorithms/compression/IVideoDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT x264VideoDecoder : public IVideoDecoder {
 public:
    x264VideoDecoder();
    ~x264VideoDecoder() override;

    bool addBytes(const std::vector<uint8_t>& bytes) override;
    VideoFrame getFrame() override;
    bool clear() override;

 private:
    std::vector<uint8_t> buffer_;
#if defined(_NVJETSON)
    void* nvmpiCtx_;
    VideoResolution receivedResolution_;
#else

    const AVCodec* codec_;
    AVCodecContext* context_;
    AVPacket* packet_;
    AVBufferRef * hwDeviceCtx_;

    AVFrame* hwFrame_;
    AVHWDeviceType selectedDevType_;
#endif
};

} // namespace compression
} // namespace algorithms
} // namespace urf
