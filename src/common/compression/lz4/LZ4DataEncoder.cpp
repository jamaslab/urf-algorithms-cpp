#ifdef __linux__
    #include <arpa/inet.h>
#elif _WIN32 || _WIN64
    #include <windows.h>
#endif

#include "urf/algorithms/compression/lz4/LZ4DataEncoder.hpp"

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("LZ4DataEncoder");
}

namespace urf {
namespace algorithms {
namespace compression {

LZ4DataEncoder::LZ4DataEncoder(bool stream)
    : stream_()
    , compressBound_(0)
    , outBuffer_()
    , useStream_(stream) {
    if (useStream_) {
        stream_ = LZ4_createStream();
    }
    compressBound_ = LZ4_COMPRESSBOUND(blockSize_);
    outBuffer_ = new char[compressBound_];
}

LZ4DataEncoder::~LZ4DataEncoder() {
    if (useStream_ && stream_) {
        LZ4_freeStream(stream_);
    }

    if (outBuffer_) {
        delete[] outBuffer_;
    }
}

std::vector<uint8_t> LZ4DataEncoder::encode(const std::vector<uint8_t>& buffer) {
    return encode(buffer.data(), buffer.size());
}

std::vector<uint8_t> LZ4DataEncoder::encode(const uint8_t* buffer, size_t bufferSize) {
    auto requiredBlocks = static_cast<uint32_t>(bufferSize / blockSize_) + 1;
    std::vector<uint8_t> outputBuffer;

    uint32_t totalSize = 0;
    // I already insert the total size in the first place for efficiency and then I update it with memcpy at the end
    uint8_t* totalSizePtr = reinterpret_cast<uint8_t*>(&totalSize);
    outputBuffer.insert(outputBuffer.begin(), totalSizePtr, totalSizePtr + sizeof(totalSize));

    for (uint32_t i = 0; i < requiredBlocks; i++) {
        size_t availableToRead = (i == (requiredBlocks - 1)) ? bufferSize % blockSize_ : blockSize_;
        int cmpBytes = 0;
        if (useStream_) {
            cmpBytes =
                LZ4_compress_fast_continue(stream_,
                                           reinterpret_cast<const char*>(buffer + i * blockSize_),
                                           outBuffer_,
                                           availableToRead,
                                           compressBound_,
                                           1);
        } else {
            cmpBytes = LZ4_compress_fast(reinterpret_cast<const char*>(buffer + i * blockSize_),
                                         outBuffer_,
                                         availableToRead,
                                         compressBound_,
                                         1);
        }

        if (cmpBytes <= 0) {
            LOGGER.warn("Could not encode bytes");
            return std::vector<uint8_t>();
        }

        pack(outputBuffer, htonl(static_cast<uint32_t>(cmpBytes)));
        outputBuffer.insert(outputBuffer.end(), outBuffer_, outBuffer_ + cmpBytes);

        totalSize += cmpBytes + sizeof(uint32_t);
    }

    totalSize = htonl(totalSize);
    std::memcpy(outputBuffer.data(), &totalSize, sizeof(totalSize));

    return outputBuffer;
}

} // namespace compression
} // namespace algorithms
} // namespace urf