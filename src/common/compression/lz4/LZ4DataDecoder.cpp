#ifdef __linux__
    #include <arpa/inet.h>
#elif _WIN32 || _WIN64
    #include <windows.h>
    #include <winsock.h>
#endif

#include "urf/algorithms/compression/lz4/LZ4DataDecoder.hpp"

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("LZ4DataDecoder");
}

namespace urf {
namespace algorithms {
namespace compression {

LZ4DataDecoder::LZ4DataDecoder(bool stream)
    : stream_()
    , compressBound_(0)
    , outBuffer_()
    , buffer_()
    , useStream_(stream) {
    if (useStream_) {
        stream_ = LZ4_createStreamDecode();
    }
    compressBound_ = LZ4_COMPRESSBOUND(blockSize_);
    outBuffer_ = new char[compressBound_];
}

LZ4DataDecoder::~LZ4DataDecoder() {
    if (useStream_ && stream_) {
        LZ4_freeStreamDecode(stream_);
    }

    if (outBuffer_) {
        delete[] outBuffer_;
    }
}

bool LZ4DataDecoder::addBytes(const std::vector<uint8_t>& bytes) {
    buffer_.insert(buffer_.end(), bytes.begin(), bytes.end());
    return true;
}

std::vector<uint8_t> LZ4DataDecoder::decode(bool clearBuffer) {
    uint32_t availableToRead;
    unpack(buffer_, 0, availableToRead);
    availableToRead = ntohl(availableToRead);

    if (buffer_.size() < availableToRead) {
        LOGGER.warn("Not enough bytes to read");
        return std::vector<uint8_t>();
    }

    std::vector<uint8_t> returnBuffer;
    size_t blockPosition = sizeof(uint32_t);
    while (blockPosition < availableToRead) {
        uint32_t encodedBlockSize;
        unpack(buffer_, blockPosition, encodedBlockSize);
        encodedBlockSize = ntohl(encodedBlockSize);
        blockPosition += sizeof(uint32_t);
        int decBytes = 0;
        if (useStream_) {
            decBytes = LZ4_decompress_safe_continue(
                stream_,
                reinterpret_cast<const char*>(buffer_.data() + blockPosition),
                outBuffer_,
                encodedBlockSize,
                blockSize_);
        } else {
            decBytes =
                LZ4_decompress_safe(reinterpret_cast<const char*>(buffer_.data() + blockPosition),
                                    outBuffer_,
                                    encodedBlockSize,
                                    blockSize_);
        }
        blockPosition += encodedBlockSize;

        if (decBytes <= 0) {
            LOGGER.warn("Failed to decode");
            return std::vector<uint8_t>();
        }

        returnBuffer.insert(returnBuffer.end(), outBuffer_, outBuffer_ + decBytes);
    }

    if (clearBuffer) {
        buffer_.erase(buffer_.begin(), buffer_.begin() + availableToRead + sizeof(uint32_t));
    }

    return returnBuffer;
}

} // namespace compression
} // namespace algorithms
} // namespace urf