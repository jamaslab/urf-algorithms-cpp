#include "BitmapEncoder.hpp"
#include <urf/common/logger/Logger.hpp>

#ifdef __linux__
#    include <arpa/inet.h>
#elif _WIN32 || _WIN64
#    include <windows.h>
#    include <winsock.h>
#endif

namespace {
auto LOGGER = urf::common::getLoggerInstance("BitmapEncoder");
}

namespace urf {
namespace algorithms {
namespace compression {

BitmapEncoder::BitmapEncoder(const VideoResolution& resolution)
    : bytes_()
    , nalDelimiter_({'\r', '\n', '\r', '\n'})
    , resolution_(resolution)
    , flushed_(false) {
    LOGGER.trace("Ctor");
}

BitmapEncoder::~BitmapEncoder() { }

bool BitmapEncoder::addFrame(const VideoFrame& frame) {
    if (flushed_) {
        LOGGER.warn("Encoder has been already flushed");
        return false;
    }

    bytes_.insert(bytes_.end(), nalDelimiter_.begin(), nalDelimiter_.end());
    pack(bytes_, htonl(frame.width()));
    pack(bytes_, htonl(frame.height()));
    auto format = frame.pixelFormat() + "~~";
    bytes_.insert(bytes_.end(), format.c_str(), format.c_str() + format.length());

    for (uint32_t i = 0; i < frame.planes(); i++) {
        bytes_.insert(bytes_.end(),
                      frame.data(i),
                      frame.data(i) + frame.stride(i) * frame.height());
    }

    return true;
}

std::vector<uint8_t> BitmapEncoder::getBytes(bool clearBuffer) {
    std::vector<uint8_t> retval = bytes_;
    if (clearBuffer)
        bytes_.clear();
    return retval;
}

bool BitmapEncoder::flush() {
    LOGGER.trace("flush");
    flushed_ = true;
    return true;
}

CompressionQuality BitmapEncoder::getCompressionQuality() {
    LOGGER.trace("getCompressionQuality");
    return CompressionQuality::Normal;
}

VideoResolution BitmapEncoder::getResolution() {
    LOGGER.trace("getResolution");
    return resolution_;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
