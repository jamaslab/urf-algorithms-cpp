#pragma once

#if defined(_WIN32) || defined(_WIN64)
#include "urf/algorithms/urf_algorithms_export.h"
#else
#define URF_ALGORITHMS_EXPORT
#endif

#include <mutex>

#include "urf/algorithms/compression/IVideoEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT BitmapEncoder : public IVideoEncoder {
 public:
    BitmapEncoder(const VideoResolution& resolution);
    ~BitmapEncoder() override;

    bool addFrame(const VideoFrame&) override;
    bool flush() override;
    std::vector<uint8_t> getBytes(bool clearBuffer = true) override;
    CompressionQuality getCompressionQuality() override;
    VideoResolution getResolution() override;

    inline std::string getName() override {
        return "bmp";
    }

 private:
    std::vector<uint8_t> bytes_;
    std::vector<uint8_t> nalDelimiter_;
    VideoResolution resolution_;

    bool flushed_;

    template <typename T>
    inline void pack(std::vector<uint8_t>& dst, T data) const {
        uint8_t* src = reinterpret_cast<uint8_t*>(&data);
        dst.insert(dst.end(), src, src + sizeof(T));
    }
};

} // namespace compression
} // namespace algorithms
} // namespace urf