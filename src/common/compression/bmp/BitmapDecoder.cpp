#include "BitmapDecoder.hpp"

#include <algorithm>

#ifdef __linux__
#    include <arpa/inet.h>
#elif _WIN32 || _WIN64
#    include <windows.h>
#    include <winsock.h>
#endif

#include <iostream>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("BitmapDecoder");
}

namespace urf {
namespace algorithms {
namespace compression {

BitmapDecoder::BitmapDecoder()
    : buffer_()
    , nalDivider_({'\r', '\n', '\r', '\n'}) {
    LOGGER.trace("CTor");
}

bool BitmapDecoder::addBytes(const std::vector<uint8_t>& bytes) {
    buffer_.insert(buffer_.end(), bytes.begin(), bytes.end());
    return true;
}

VideoFrame BitmapDecoder::getFrame() {
    auto first =
        std::search(buffer_.begin(), buffer_.end(), nalDivider_.begin(), nalDivider_.end());
    if (first == buffer_.end()) {
        LOGGER.warn("No frames in buffer");
        return VideoFrame();
    }

    auto second = std::search(
        first + nalDivider_.size(), buffer_.end(), nalDivider_.begin(), nalDivider_.end());

    uint32_t width, height;

    size_t start = std::distance(buffer_.begin(), first) + nalDivider_.size();
    unpack(buffer_, start, width);
    width = ntohl(width);

    unpack(buffer_, start + 4, height);
    height = ntohl(height);

    std::string formatDivider = "~~";
    auto pixFormatPos =
        std::search(buffer_.begin() + start, second, formatDivider.begin(), formatDivider.end());
    std::string format(first + nalDivider_.size() + 8, pixFormatPos);
    VideoFrame res(width, height, format);

    start = std::distance(buffer_.begin(), pixFormatPos) + formatDivider.size();
    for (uint32_t i = 0; i < res.planes(); i++) {
        std::memcpy(const_cast<uint8_t*>(res.data(i)), buffer_.data() + start, res.stride(i) * res.height());
        start += res.stride(i) * res.height();
    }

    if (!res.empty())
        buffer_.erase(buffer_.begin(), second);

    return res;
}

bool BitmapDecoder::clear() {
    LOGGER.trace("clear()");
    buffer_.clear();
    return true;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
