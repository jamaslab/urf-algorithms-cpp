#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/algorithms/urf_algorithms_export.h"
#else
    #define URF_ALGORITHMS_EXPORT
#endif
#include <cstring>
#include <string>

#include "urf/algorithms/compression/IVideoDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT BitmapDecoder : public IVideoDecoder {
 public:
    BitmapDecoder();
    ~BitmapDecoder() override = default;

    bool addBytes(const std::vector<uint8_t>& bytes) override;
    VideoFrame getFrame() override;
    bool clear() override;

 private:
    std::vector<uint8_t> buffer_;
    std::vector<uint8_t> nalDivider_;

    template <typename T>
    inline void unpack(const std::vector<uint8_t>& src, size_t index, T& data) {
        std::memcpy(&data, src.data()+index, sizeof(T));
    }
};

} // namespace compression
} // namespace algorithms
} // namespace urf
