#include <stdexcept>

#include "urf/algorithms/compression/DataCompressionFactory.hpp"

#include "urf/algorithms/compression/lz4/LZ4DataDecoder.hpp"
#include "urf/algorithms/compression/lz4/LZ4DataEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

std::unique_ptr<IDataDecoder> DataCompressionFactory::getDecoder(const std::string& decoder,
                                                                 bool stream) {
    if (decoder == "lz4") {
        return std::move(std::make_unique<LZ4DataDecoder>(stream));
    }

    throw std::invalid_argument("Invalid decoder requested. Available decoders: lz4");
}

std::unique_ptr<IDataEncoder> DataCompressionFactory::getEncoder(const std::string& encoder,
                                                                 bool stream) {
    if (encoder == "lz4") {
        return std::move(std::make_unique<LZ4DataEncoder>(stream));
    }

    throw std::invalid_argument("Invalid encoder requested. Available decoders: lz4");
}

} // namespace compression
} // namespace algorithms
} // namespace urf
