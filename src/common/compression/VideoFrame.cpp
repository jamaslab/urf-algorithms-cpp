#include "urf/algorithms/compression/VideoFrame.hpp"

#define __STDC_CONSTANT_MACROS
#ifdef __cplusplus
extern "C" {
#endif
#include <libavutil/frame.h>
#include <libavutil/pixdesc.h>
#include <libswresample/swresample.h>
#include <libswscale/swscale.h>
#ifdef __cplusplus
};
#endif

#include <opencv2/core/mat.hpp>

#include <chrono>
#include <iostream>

namespace urf {
namespace algorithms {
namespace compression {

VideoResolution::VideoResolution()
    : width(0)
    , height(0) { }
VideoResolution::VideoResolution(uint32_t w, uint32_t h)
    : width(w)
    , height(h) { }

bool VideoResolution::empty() const {
    return width == 0 && height == 0;
}

std::ostream& operator<<(std::ostream& os, const VideoResolution& res) {
    os << "[" << res.width << ", " << res.height << "]";
    return os;
}

bool operator==(const VideoResolution& lhs, const VideoResolution& rhs) {
    return lhs.width == rhs.width && lhs.height == rhs.height;
}

struct VideoFrame::frame_wrapper {
    AVFrame* frame;
};

VideoFrame::VideoFrame()
    : _frame(new frame_wrapper) {
    _frame->frame = av_frame_alloc();
}

VideoFrame::VideoFrame(uint32_t width, uint32_t height, const std::string& format)
    : _frame(new frame_wrapper) {
    _frame->frame = av_frame_alloc();

    auto targetFormat = av_get_pix_fmt(format.c_str());
    if (targetFormat == AV_PIX_FMT_NONE) {
        throw std::invalid_argument("Invalid pixel format");
    }

    _frame->frame->width = width;
    _frame->frame->height = height;
    _frame->frame->format = targetFormat;

    av_frame_get_buffer(_frame->frame, 1);
    av_frame_make_writable(_frame->frame);
}

VideoFrame::VideoFrame(const VideoFrame& frame)
    : _frame(new frame_wrapper) {
    _frame->frame = av_frame_clone(frame._frame->frame);
}

VideoFrame::VideoFrame(VideoFrame&& frame)
    : _frame(new frame_wrapper) {
    if (frame._frame->frame) {
        _frame->frame = av_frame_alloc();
        av_frame_move_ref(_frame->frame, frame._frame->frame);
    }
}

template <>
VideoFrame::VideoFrame(AVFrame* frame)
    : _frame(new frame_wrapper) {
    if (frame) {
        _frame->frame = av_frame_alloc();
        av_frame_ref(_frame->frame, frame);
    }
}

VideoFrame::~VideoFrame() {
    if (_frame->frame) {
        av_frame_free(&_frame->frame);
    }
}

uint32_t VideoFrame::width() const {
    return _frame->frame->width;
}

uint32_t VideoFrame::height() const {
    return _frame->frame->height;
}

uint32_t VideoFrame::stride(uint32_t plane) const {
    if (plane >= AV_NUM_DATA_POINTERS) {
        return 0;
    }

    return _frame->frame->linesize[plane];
}

uint32_t VideoFrame::planes() const {
    for (int i = 0; i < AV_NUM_DATA_POINTERS; i++) {
        if (_frame->frame->linesize[i] == 0) {
            return i;
        }
    }

    return 0;
}

bool VideoFrame::empty() const {
    return width() == 0 && height() == 0;
}

std::string VideoFrame::pixelFormat() const {
    return std::string(av_get_pix_fmt_name(static_cast<AVPixelFormat>(_frame->frame->format)));
}

const uint8_t* VideoFrame::data(uint32_t plane) const {
    if (plane >= AV_NUM_DATA_POINTERS) {
        return nullptr;
    }

    return static_cast<const uint8_t*>(_frame->frame->data[plane]);
}

template <>
AVFrame*
VideoFrame::get<AVFrame*>(uint32_t width, uint32_t height, const std::string& format) const {
    if ((width == static_cast<uint32_t>(_frame->frame->width)) &&
        (height == static_cast<uint32_t>(_frame->frame->height)) && (format == pixelFormat())) {
        return av_frame_clone(_frame->frame);
    }

    auto targetFormat = av_get_pix_fmt(format.c_str());
    if (targetFormat == AV_PIX_FMT_NONE) {
        return nullptr;
    }

    auto swsContext = sws_getContext(_frame->frame->width,
                                     _frame->frame->height,
                                     static_cast<AVPixelFormat>(_frame->frame->format),
                                     width,
                                     height,
                                     targetFormat,
                                     SWS_BILINEAR,
                                     0,
                                     0,
                                     0);

    auto retval = av_frame_alloc();
    retval->width = width;
    retval->height = height;
    retval->format = targetFormat;

    av_frame_get_buffer(retval, 0);
    av_frame_make_writable(retval);
    sws_scale(swsContext,
              (const uint8_t* const*)_frame->frame->data,
              _frame->frame->linesize,
              0,
              _frame->frame->height,
              retval->data,
              retval->linesize);

    sws_freeContext(swsContext);
    return retval;
}

template <>
VideoFrame
VideoFrame::get<VideoFrame>(uint32_t width, uint32_t height, const std::string& format) const {
    auto frame = get<AVFrame*>(width, height, format);

    return VideoFrame(frame);
}

template <>
cv::Mat VideoFrame::get(uint32_t width, uint32_t height, const std::string& format) const {
    AVPixelFormat targetFormat = AV_PIX_FMT_NONE;
    cv::Mat res;
    if ((format == "bgr24") || (format == "")) {
        targetFormat = AV_PIX_FMT_BGR24;
        res = cv::Mat(height, width, CV_8UC3);
    } else if (format == "gray8") {
        targetFormat = AV_PIX_FMT_GRAY8;
        res = cv::Mat(height, width, CV_8UC1);
    } else if (format == "gray16") {
        targetFormat = AV_PIX_FMT_GRAY16;
        res = cv::Mat(height, width, CV_16UC1);
    } else {
        throw std::invalid_argument("For the moment, the provided pixel format is not supported for this conversion");
    }

    auto swsContext = sws_getContext(_frame->frame->width,
                                     _frame->frame->height,
                                     static_cast<AVPixelFormat>(_frame->frame->format),
                                     width,
                                     height,
                                     targetFormat,
                                     SWS_BILINEAR,
                                     0,
                                     0,
                                     0);

    int linesize[AV_NUM_DATA_POINTERS];
    linesize[0] = static_cast<int>(res.step);
    sws_scale(swsContext,
              (const uint8_t* const*)_frame->frame->data,
              _frame->frame->linesize,
              0,
              _frame->frame->height,
              &res.data,
              linesize);

    sws_freeContext(swsContext);
    return res;
}

const void* VideoFrame::ptr() const {
    return static_cast<void*>(av_frame_clone(_frame->frame));
}

VideoFrame& VideoFrame::operator=(VideoFrame&& rhs) {
    if (_frame->frame) {
        av_frame_unref(_frame->frame);
    }

    if (rhs._frame->frame) {
        _frame->frame = av_frame_alloc();
        av_frame_move_ref(_frame->frame, rhs._frame->frame);
    }

    return *this;
}

VideoFrame& VideoFrame::operator=(const VideoFrame& rhs) {
    if (_frame->frame) {
        av_frame_unref(_frame->frame);
    }

    _frame->frame = av_frame_clone(rhs._frame->frame);
    return *this;
}

template <>
VideoFrame
VideoFrame::from(const cv::Mat& frame, uint32_t width, uint32_t height, const std::string& format) {
    VideoFrame retval;

    if (width == 0) {
        width = frame.cols;
    }

    if (height == 0) {
        height = frame.rows;
    }

    auto targetFormat = av_get_pix_fmt(format.c_str());
    if (targetFormat == AV_PIX_FMT_NONE) {
        targetFormat = AV_PIX_FMT_BGR24;
    }

    auto swsContext = sws_getContext(frame.cols,
                                     frame.rows,
                                     AV_PIX_FMT_BGR24,
                                     width,
                                     height,
                                     targetFormat,
                                     SWS_BILINEAR,
                                     0,
                                     0,
                                     0);
    retval._frame->frame->width = width;
    retval._frame->frame->height = height;
    retval._frame->frame->format = targetFormat;

    av_frame_get_buffer(retval._frame->frame, 0);
    av_frame_make_writable(retval._frame->frame);

    int linesize[AV_NUM_DATA_POINTERS];
    linesize[0] = static_cast<int>(frame.step);
    sws_scale(swsContext,
              &frame.data,
              linesize,
              0,
              frame.rows,
              retval._frame->frame->data,
              retval._frame->frame->linesize);
    sws_freeContext(swsContext);

    return retval;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
