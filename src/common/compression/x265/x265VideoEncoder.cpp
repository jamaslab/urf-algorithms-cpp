#include "x265VideoEncoder.hpp"

#include <opencv2/imgproc.hpp>
#include <x265.h>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("x265VideoEncoder");

#ifdef av_err2str
#    undef av_err2str
#    include <string>
av_always_inline std::string av_err2string(int errnum) {
    char str[AV_ERROR_MAX_STRING_SIZE];
    return av_make_error_string(str, AV_ERROR_MAX_STRING_SIZE, errnum);
}
#    define av_err2str(err) av_err2string(err).c_str()
#endif // av_err2str

} // namespace

namespace urf {
namespace algorithms {
namespace compression {

x265VideoEncoder::x265VideoEncoder(VideoResolution resolution,
                                   uint16_t fps,
                                   CompressionQuality quality,
                                   bool zeroLatency)
    : resolution_(resolution)
    , quality_(quality)
    , zeroLatency_(zeroLatency)
    , flushed_(false)
    , flowBytes_(false) {
    LOGGER.trace("Ctor");

    pts_ = 0;
    av_log_set_level(AV_LOG_QUIET);
    codec_ = avcodec_find_encoder(AV_CODEC_ID_H265);
    if (!codec_) {
        LOGGER.warn("Unsupported x265 encoder");
        throw std::runtime_error("Unsupported x265 encoder");
    }

    codecContext_ = avcodec_alloc_context3(codec_);
    if (!codecContext_) {
        LOGGER.warn("Failed to allocate context");
        throw std::runtime_error("Failed to allocate context");
    }

    int compressionFactor = static_cast<int>(51 - ((static_cast<int>(quality) * 1.25) * 5.1));
    codecContext_->width = resolution.width;
    codecContext_->height = resolution.height;
    codecContext_->time_base = {1, fps};
    codecContext_->framerate = {fps, 1};
    codecContext_->gop_size = 250;
    codecContext_->max_b_frames = 0;
    codecContext_->pix_fmt = AV_PIX_FMT_YUV420P;
    codecContext_->codec_id = AV_CODEC_ID_H265;
    codecContext_->codec_type = AVMEDIA_TYPE_VIDEO;
    codecContext_->qmin = compressionFactor;
    codecContext_->qmax = compressionFactor;

    AVDictionary* dict = nullptr;
    av_dict_set(&dict, "preset", x265_preset_names[static_cast<int>(quality_)], 1);
    av_dict_set(&dict, "tune", "zerolatency", zeroLatency);
    av_dict_set(&dict, "profile", "main", 0);
    avp_ = av_packet_alloc();
    if (!avp_) {
        LOGGER.warn("Failed to allocate frames and packet");
        throw std::runtime_error("Failed to allocate frames and packet");
    }

    avp_ = av_packet_alloc();
    auto retval = avcodec_open2(codecContext_, codec_, &dict);
    if (retval < 0) {
        LOGGER.warn("Failed to open codec: {}", av_err2str(retval));
        throw std::runtime_error("Failed to open codec");
    }
}

x265VideoEncoder::~x265VideoEncoder() {
    if (codecContext_)
        avcodec_free_context(&codecContext_);

    if (avp_)
        av_packet_free(&avp_);
}

bool x265VideoEncoder::addFrame(const VideoFrame& frame) {
    if (flushed_) {
        LOGGER.warn("Already flushed");
        return false;
    }

    auto avFrame = frame.get<VideoFrame>(resolution_.width, resolution_.height, "yuv420p");
    int r = avcodec_send_frame(codecContext_, static_cast<const AVFrame*>(avFrame.ptr()));
    if (r >= 0) {
        r = avcodec_receive_packet(codecContext_, avp_);
        if (r == 0) {
            flowBytes_.insert(flowBytes_.end(),
                              reinterpret_cast<uint8_t*>(avp_->data),
                              reinterpret_cast<uint8_t*>(avp_->data) + avp_->size);
            return true;
        }

        if (r == AVERROR(EAGAIN) || r == AVERROR_EOF) {
            return false;
        }
    }
    return false;
}

std::vector<uint8_t> x265VideoEncoder::getBytes(bool clearBuffer) {
    if (clearBuffer) {
        auto copy = flowBytes_;
        flowBytes_.clear();
        return copy;
    }

    return flowBytes_;
}

bool x265VideoEncoder::flush() {
    LOGGER.trace("flush()");
    if (zeroLatency_) {
        LOGGER.warn("Can't flush in zerolatency mode");
        return false;
    }

    int got_output = -1;
    int ret = -1;
    do {
        ret = avcodec_receive_packet(codecContext_, avp_);
        if (ret == 0) {
            flowBytes_.insert(flowBytes_.end(),
                              reinterpret_cast<uint8_t*>(avp_->data),
                              reinterpret_cast<uint8_t*>(avp_->data) + avp_->size);
        }
    } while ((ret != AVERROR(EAGAIN)) && (ret != AVERROR_EOF));
    flushed_ = true;
    
    return true;
}

CompressionQuality x265VideoEncoder::getCompressionQuality() {
    LOGGER.trace("getCompressionQuality");
    return quality_;
}

VideoResolution x265VideoEncoder::getResolution() {
    LOGGER.trace("getResolution");
    return resolution_;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
