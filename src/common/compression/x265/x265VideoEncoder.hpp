#pragma once

#if defined(_WIN32) || defined(_WIN64)
#include "urf/algorithms/urf_algorithms_export.h"
#else
#define URF_ALGORITHMS_EXPORT
#endif

#include <mutex>

#define __STDC_CONSTANT_MACROS
#ifdef __cplusplus
extern "C" {
#endif
#include <libavcodec/avcodec.h>
#include <libavdevice/avdevice.h>
#include <libavformat/avformat.h>
#include <libavutil/mathematics.h>
#include <libavutil/time.h>
#include <libswresample/swresample.h>
#include <libswscale/swscale.h>
#ifdef __cplusplus
};
#endif

#include <opencv2/core/mat.hpp>

#include "urf/algorithms/compression/IVideoEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {


class URF_ALGORITHMS_EXPORT x265VideoEncoder : public IVideoEncoder {
 public:
    x265VideoEncoder() = delete;
    x265VideoEncoder(VideoResolution resolution, uint16_t fps, CompressionQuality quality, bool zeroLatency);
    ~x265VideoEncoder() override;

    bool addFrame(const VideoFrame&) override;
    bool flush() override;
    std::vector<uint8_t> getBytes(bool clearBuffer = true) override;
    CompressionQuality getCompressionQuality() override;
    VideoResolution getResolution() override;

    inline std::string getName() override {
        return "x265";
    }

 private:
    VideoResolution resolution_;
    CompressionQuality quality_;
    bool zeroLatency_, flushed_;

    const AVCodec* codec_;
    AVCodecContext* codecContext_;
    AVPacket* avp_;
    int pts_;

    std::vector<uint8_t> flowBytes_;
};

} // namespace compression
} // namespace algorithms
} // namespace urf