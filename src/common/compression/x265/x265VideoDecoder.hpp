#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/algorithms/urf_algorithms_export.h"
#else
#    define URF_ALGORITHMS_EXPORT
#endif

#    ifdef __cplusplus
extern "C" {
#    endif
#    include "libavcodec/avcodec.h"
#    include "libavfilter/avfilter.h"
#    include "libavfilter/buffersink.h"
#    include "libavfilter/buffersrc.h"
#    include "libavformat/avformat.h"
#    include "libavutil/avstring.h"
#    include "libavutil/channel_layout.h"
#    include "libavutil/dict.h"
#    include "libavutil/fifo.h"
#    include "libavutil/imgutils.h"
#    include "libavutil/intreadwrite.h"
#    include "libavutil/mathematics.h"
#    include "libavutil/opt.h"
#    include "libavutil/parseutils.h"
#    include "libavutil/pixdesc.h"
#    include "libavutil/samplefmt.h"
#    include "libswresample/swresample.h"
#    include <libswscale/swscale.h>
#    ifdef __cplusplus
};
#    endif

#include <opencv2/core/mat.hpp>
#include <string>

#include "urf/algorithms/compression/IVideoDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT x265VideoDecoder : public IVideoDecoder {
 public:
    x265VideoDecoder();
    ~x265VideoDecoder() override;

    bool addBytes(const std::vector<uint8_t>& bytes) override;
    VideoFrame getFrame() override;
    bool clear() override;

 private:
    std::vector<uint8_t> buffer_;

    const AVCodec* codec_;
    AVCodecContext* context_;
    AVPacket* packet_;
    AVBufferRef * hwDeviceCtx_;

    AVFrame *hwFrame_;
    AVHWDeviceType selectedDevType_;
};

} // namespace compression
} // namespace algorithms
} // namespace urf
