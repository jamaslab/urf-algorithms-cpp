#include "JpegVideoEncoder.hpp"

#include <opencv2/imgcodecs.hpp>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("JpegVideoEncoder");
}

namespace urf {
namespace algorithms {
namespace compression {

JpegVideoEncoder::JpegVideoEncoder(const VideoResolution& resolution, CompressionQuality quality)
    : quality_(quality)
    , compression_params_()
    , bytes_()
    , nalDelimiter_({'\r', '\n', '\r', '\n'})
    , flushed_(false)
    , resolution_(resolution) {
    LOGGER.trace("Ctor");
    uint32_t compression = (static_cast<uint32_t>(quality) * 10) + 10;
    compression_params_.push_back(cv::IMWRITE_JPEG_QUALITY);
    compression_params_.push_back(compression);
}

JpegVideoEncoder::~JpegVideoEncoder() { }

bool JpegVideoEncoder::addFrame(const VideoFrame& frame) {
    if (flushed_) {
        LOGGER.warn("Encoder has been already flushed");
        return false;
    }

    auto mat = frame.get<cv::Mat>(resolution_.width, resolution_.height);
    std::vector<uint8_t> buf;
    cv::imencode(".jpeg", mat, buf, compression_params_);
    bytes_.insert(bytes_.end(), nalDelimiter_.begin(), nalDelimiter_.end());
    bytes_.insert(bytes_.end(), buf.begin(), buf.end());
    return true;
}

std::vector<uint8_t> JpegVideoEncoder::getBytes(bool clearBuffer) {
    std::vector<uint8_t> retval = bytes_;
    if (clearBuffer)
        bytes_.clear();
    return retval;
}

bool JpegVideoEncoder::flush() {
    LOGGER.trace("flush");
    flushed_ = true;
    return true;
}

CompressionQuality JpegVideoEncoder::getCompressionQuality() {
    LOGGER.trace("getCompressionQuality");
    return quality_;
}

VideoResolution JpegVideoEncoder::getResolution() {
    LOGGER.trace("getResolution");
    return resolution_;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
