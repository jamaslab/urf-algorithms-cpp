#include "JpegVideoDecoder.hpp"

#include <algorithm>
#include <opencv2/imgcodecs.hpp>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("JpegVideoDecoder");
}

namespace urf {
namespace algorithms {
namespace compression {

JpegVideoDecoder::JpegVideoDecoder()
    : buffer_()
    , nalDivider_({'\r', '\n', '\r', '\n'}) {
    LOGGER.trace("CTor");
}

bool JpegVideoDecoder::addBytes(const std::vector<uint8_t>& bytes) {
    buffer_.insert(buffer_.end(), bytes.begin(), bytes.end());
    return true;
}

VideoFrame JpegVideoDecoder::getFrame() {
    auto first =
        std::search(buffer_.begin(), buffer_.end(), nalDivider_.begin(), nalDivider_.end());
    if (first == buffer_.end()) {
        LOGGER.warn("No frames in buffer");
        return VideoFrame();
    }

    auto second = std::search(
        first + nalDivider_.size(), buffer_.end(), nalDivider_.begin(), nalDivider_.end());
    std::vector<char> buffer_vec(first + nalDivider_.size(), second);

    auto mat = cv::imdecode(buffer_vec, cv::IMREAD_COLOR);
    if (!mat.empty())
        buffer_.erase(buffer_.begin(), second);

    return VideoFrame::from(mat);
}

bool JpegVideoDecoder::clear() {
    LOGGER.trace("clear()");
    buffer_.clear();
    return true;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
