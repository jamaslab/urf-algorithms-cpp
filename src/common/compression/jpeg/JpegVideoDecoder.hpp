#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/algorithms/urf_algorithms_export.h"
#else
    #define URF_ALGORITHMS_EXPORT
#endif

#include <string>

#if defined(_NVJETSON)
    #include "NvJpegDecoder.h"
    #include "NvVideoConverter.h"
#endif

#include "urf/algorithms/compression/IVideoDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT JpegVideoDecoder : public IVideoDecoder {
 public:
    JpegVideoDecoder();
    ~JpegVideoDecoder() override = default;

    bool addBytes(const std::vector<uint8_t>& bytes) override;
    VideoFrame getFrame() override;
    bool clear() override;

 private:
    std::vector<uint8_t> buffer_;
    std::vector<uint8_t> nalDivider_;

#if defined(_NVJETSON)
    std::unique_ptr<NvJPEGDecoder> decoder_;
    unsigned long outBufSize_;
    unsigned char* outBuf_;
#endif
};

} // namespace compression
} // namespace algorithms
} // namespace urf
