#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/algorithms/urf_algorithms_export.h"
#else
    #define URF_ALGORITHMS_EXPORT
#endif

#include <string>

#if defined(_NVJETSON)
#include "NvJpegEncoder.h"
#include "NvVideoConverter.h"
#ifdef __cplusplus
extern "C" {
#endif
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#ifdef __cplusplus
};
#endif
#endif

#include "urf/algorithms/compression/IVideoEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT JpegVideoEncoder : public IVideoEncoder {
 public:
    JpegVideoEncoder() = delete;
    JpegVideoEncoder(const VideoResolution& resolution, CompressionQuality quality);
    ~JpegVideoEncoder() override;
    bool addFrame(const VideoFrame&) override;
    std::vector<uint8_t> getBytes(bool clearBuffer = true) override;
    bool flush() override;
    CompressionQuality getCompressionQuality() override;
    VideoResolution getResolution() override;
    inline std::string getName() override {
        return "jpeg";
    }

 private:
    CompressionQuality quality_;
    std::vector<int> compression_params_;
    std::vector<uint8_t> bytes_;
    std::vector<uint8_t> nalDelimiter_;
    bool flushed_;
    VideoResolution resolution_;

#if defined(_NVJETSON)
    std::unique_ptr<NvJPEGEncoder> encoder_;
    std::unique_ptr<NvBuffer> buffer_;
    unsigned long outBufSize_;
    unsigned char* outBuf_;
#endif
};

} // namespace compression
} // namespace algorithms
} // namespace urf
