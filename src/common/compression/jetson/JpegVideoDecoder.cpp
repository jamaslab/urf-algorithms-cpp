#include "common/compression/jpeg/JpegVideoDecoder.hpp"

#include <algorithm>
#include <opencv2/imgproc.hpp>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("NvJpegVideoDecoder");
}

namespace urf {
namespace algorithms {
namespace compression {

JpegVideoDecoder::JpegVideoDecoder()
    : buffer_()
    , nalDivider_({'\r', '\n', '\r', '\n'})
    , decoder_(NvJPEGDecoder::createJPEGDecoder("jpegdec")) {
    LOGGER.trace("CTor");
}

bool JpegVideoDecoder::addBytes(const std::vector<uint8_t>& bytes) {
    buffer_.insert(buffer_.end(), bytes.begin(), bytes.end());
    return true;
}

VideoFrame JpegVideoDecoder::getFrame() {
    auto first =
        std::search(buffer_.begin(), buffer_.end(), nalDivider_.begin(), nalDivider_.end());
    if (first == buffer_.end()) {
        LOGGER.warn("No frames in buffer");
        return VideoFrame();
    }

    auto second = std::search(
        first + nalDivider_.size(), buffer_.end(), nalDivider_.begin(), nalDivider_.end());

    uint32_t width, height, pixfmt;
    NvBuffer* nvBuffer;
    auto ret =
        decoder_->decodeToBuffer(&nvBuffer,
                                 reinterpret_cast<unsigned char*>(&(*first) + nalDivider_.size()),
                                 buffer_.size(),
                                 &pixfmt,
                                 &width,
                                 &height);

    if (ret == -1) {
        LOGGER.warn("Failed to decode");
        return VideoFrame();
    }

    VideoFrame frame(width, height, "yuv420p");
    memcpy(const_cast<uint8_t*>(frame.data(0)), nvBuffer->planes[0].data, nvBuffer->planes[0].length);
    memcpy(const_cast<uint8_t*>(frame.data(1)), nvBuffer->planes[1].data, nvBuffer->planes[1].length);
    memcpy(const_cast<uint8_t*>(frame.data(2)), nvBuffer->planes[2].data, nvBuffer->planes[2].length);
    nvBuffer->deallocateMemory();

    if (!frame.empty())
        buffer_.erase(buffer_.begin(), second);

    return frame;
}

bool JpegVideoDecoder::clear() {
    LOGGER.trace("clear()");
    buffer_.clear();
    return true;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
