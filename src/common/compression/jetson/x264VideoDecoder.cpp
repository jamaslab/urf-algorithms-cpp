#include "common/compression/x264/x264VideoDecoder.hpp"

#include <algorithm>
#include <iostream>
#include <opencv2/imgproc.hpp>
#include <poll.h>

#include "nvmpi/nvmpi.h"

#include <urf/common/logger/Logger.hpp>

#define MAX_BUFFERS 10

namespace {
auto LOGGER = urf::common::getLoggerInstance("x264VideoDecoder");
constexpr uint32_t CHUNK_SIZE = 4000000;
constexpr uint32_t CAPTURE_BUFFER_COUNT = 5;

} // namespace

namespace urf {
namespace algorithms {
namespace compression {

x264VideoDecoder::x264VideoDecoder()
    : buffer_()
    , nvmpiCtx_()
    , receivedResolution_() {
    LOGGER.trace("CTor");
    nvmpiCtx_ = reinterpret_cast<void*>(
        nvmpi_create_decoder(nvCodingType::NV_VIDEO_CodingH264, nvPixFormat::NV_PIX_YUV420));
}

x264VideoDecoder::~x264VideoDecoder() {
    nvmpi_decoder_close(reinterpret_cast<nvmpictx*>(nvmpiCtx_));
}

bool x264VideoDecoder::addBytes(const std::vector<uint8_t>& bytes) {
    nvmpi_decoder_put_packet(reinterpret_cast<nvmpictx*>(nvmpiCtx_), bytes);
    return true;
}

VideoFrame x264VideoDecoder::getFrame() {
    nvFrame frame;

    bool waitForFrame = (receivedResolution_.width != 0) && (receivedResolution_.height != 0);
    if (nvmpi_decoder_get_frame(reinterpret_cast<nvmpictx*>(nvmpiCtx_), &frame, waitForFrame) ==
        -1) {
        return VideoFrame{};
    }

    receivedResolution_.width = frame.width;
    receivedResolution_.height = frame.height;
    VideoFrame retval(frame.width, frame.height, "yuv420p");

    std::memcpy(const_cast<uint8_t*>(retval.data(0)), frame.payload[0], frame.linesize[0] * frame.height);
    std::memcpy(const_cast<uint8_t*>(retval.data(1)), frame.payload[1], frame.linesize[1] * frame.height/2);
    std::memcpy(const_cast<uint8_t*>(retval.data(2)), frame.payload[2], frame.linesize[2] * frame.height/2);
    return retval;
}

bool x264VideoDecoder::clear() {
    LOGGER.trace("clear()");
    buffer_.clear();
    return true;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
