#ifndef TEGRA_ACCELERATE
#define TEGRA_ACCELERATE
#endif

#include "common/compression/jpeg/JpegVideoEncoder.hpp"

#include <cstring>
#include <opencv2/imgproc.hpp>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("NvJpegVideoEncoder");
}

namespace urf {
namespace algorithms {
namespace compression {

JpegVideoEncoder::JpegVideoEncoder(const VideoResolution& resolution, CompressionQuality quality)
    : quality_(quality)
    , compression_params_()
    , bytes_()
    , nalDelimiter_({'\r', '\n', '\r', '\n'})
    , flushed_(false)
    , resolution_(resolution)
    , encoder_(NvJPEGEncoder::createJPEGEncoder("jpeg_enc"))
    , buffer_(new NvBuffer(V4L2_PIX_FMT_YUV420M, resolution_.width, resolution_.height, 0))
    , outBufSize_(resolution_.width * resolution.height * 3 / 2)
    , outBuf_() {
    LOGGER.trace("Ctor");
    if (!encoder_) {
        throw std::runtime_error("Could not create jpeg encoder");
    }

    outBuf_ = new unsigned char[outBufSize_];
}

JpegVideoEncoder::~JpegVideoEncoder() {
    delete[] outBuf_;
}

bool JpegVideoEncoder::addFrame(const VideoFrame& frame) {
    if (flushed_) {
        LOGGER.warn("Encoder has been already flushed");
        return false;
    }

    auto converted = frame.get<VideoFrame>(frame.width(), frame.height(), "yuv420p");
    buffer_->planes[0].data = const_cast<unsigned char*>(converted.data(0));
    buffer_->planes[1].data = const_cast<unsigned char*>(converted.data(1));
    buffer_->planes[2].data = const_cast<unsigned char*>(converted.data(2));

    buffer_->planes[0].bytesused = converted.stride(0)*converted.height();
    buffer_->planes[1].bytesused = converted.stride(1)*converted.height()/2;
    buffer_->planes[2].bytesused = converted.stride(2)*converted.height()/2;

    auto out_buf_size = outBufSize_;
    if (encoder_->encodeFromBuffer(
            *buffer_, JCS_YCbCr, &outBuf_, out_buf_size, (static_cast<int>(quality_) + 1) * 10) ==
        -1) {
        return false;
    }
    
    bytes_.insert(bytes_.end(), nalDelimiter_.begin(), nalDelimiter_.end());
    bytes_.insert(bytes_.end(), outBuf_, outBuf_ + out_buf_size);
    return true;
}

std::vector<uint8_t> JpegVideoEncoder::getBytes(bool clearBuffer) {
    std::vector<uint8_t> retval = bytes_;
    if (clearBuffer)
        bytes_.clear();
    return retval;
}

bool JpegVideoEncoder::flush() {
    LOGGER.trace("flush");
    flushed_ = true;
    return true;
}

CompressionQuality JpegVideoEncoder::getCompressionQuality() {
    LOGGER.trace("getCompressionQuality");
    return quality_;
}

VideoResolution JpegVideoEncoder::getResolution() {
    LOGGER.trace("getResolution");
    return resolution_;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
