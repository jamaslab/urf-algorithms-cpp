#include "common/compression/x264/x264VideoEncoder.hpp"

#include <opencv2/imgproc.hpp>

#include "NvVideoEncoder.h"
#include "nvbuf_utils.h"

#include <iostream>
#include <queue>

#include <urf/common/logger/Logger.hpp>

namespace urf {
namespace algorithms {
namespace compression {

struct nvmpictx {
    std::unique_ptr<NvVideoEncoder> enc;
    bool dqThreadStarted = false;
    bool flushed = false;
    uint index = 0;

    std::mutex bytesMtxQueue;
    std::queue<std::vector<uint8_t>> bytes;
};

} // namespace compression
} // namespace algorithms
} // namespace urf

namespace {
auto LOGGER = urf::common::getLoggerInstance("x264VideoEncoder");

constexpr uint32_t CHUNK_SIZE = 2 * 1024 * 1024;

static bool encoder_capture_plane_dq_callback(struct v4l2_buffer* v4l2_buf,
                                              NvBuffer* buffer,
                                              NvBuffer*,
                                              void* arg) {
    auto ctx = reinterpret_cast<urf::algorithms::compression::nvmpictx*>(arg);

    if (v4l2_buf == NULL) {
        LOGGER.error("Error while dequeing buffer from output plane");
        return false;
    }

    if (buffer->planes[0].bytesused == 0) {
        LOGGER.error("Got 0 size buffer in capture");
        return false;
    }
    {
        std::scoped_lock lock(ctx->bytesMtxQueue);
        ctx->bytes.push(std::vector<uint8_t>(buffer->planes[0].data,
                                             buffer->planes[0].data + buffer->planes[0].bytesused));
    }

    if (ctx->enc->capture_plane.qBuffer(*v4l2_buf, NULL) < 0) {
        LOGGER.error("Error while Qing buffer at capture plane");
        return false;
    }

    return true;
}

} // namespace

namespace urf {
namespace algorithms {
namespace compression {

x264VideoEncoder::x264VideoEncoder(VideoResolution resolution,
                                   uint16_t fps,
                                   CompressionQuality quality,
                                   bool zeroLatency)
    : resolution_(resolution)
    , quality_(quality)
    , zeroLatency_(zeroLatency)
    , encoder_(new nvmpictx)
    , flowBytes_() {
    LOGGER.trace("Ctor");
    encoder_->enc.reset(NvVideoEncoder::createVideoEncoder("enc0"));
    if (!encoder_->enc) {
        LOGGER.error("Failed to createVideoEncoder");
        throw std::runtime_error("Failed to createVideoEncoder");
    }

    if (encoder_->enc->setCapturePlaneFormat(
            V4L2_PIX_FMT_H264, resolution.width, resolution.height, CHUNK_SIZE) < 0) {
        LOGGER.error("Failed to setCapturePlaneFormat");
        throw std::runtime_error("Failed to setCapturePlaneFormat");
    }

    if (encoder_->enc->setOutputPlaneFormat(
            V4L2_PIX_FMT_YUV420M, resolution.width, resolution.height) < 0) {
        LOGGER.error("Failed to setOutputPlaneFormat");
        throw std::runtime_error("Failed to setOutputPlaneFormat");
    }
    float compressionFactor = (static_cast<int>(quality) + 2) / 10.0f;
    if (encoder_->enc->setBitrate(resolution.width * resolution.height * fps * 2 * 0.07 *
                                  compressionFactor) < 0) {
        LOGGER.error("Failed to setBitrate");
        throw std::runtime_error("Failed to setBitrate");
    }

    v4l2_enc_hw_preset_type hwPresetType = V4L2_ENC_HW_PRESET_MEDIUM;
    switch (quality) {
    case CompressionQuality::Ultrafast:
        hwPresetType = V4L2_ENC_HW_PRESET_ULTRAFAST;
        break;
    case CompressionQuality::Superfast:
        hwPresetType = V4L2_ENC_HW_PRESET_ULTRAFAST;
        break;
    case CompressionQuality::Veryfast:
        hwPresetType = V4L2_ENC_HW_PRESET_ULTRAFAST;
        break;
    case CompressionQuality::Faster:
        hwPresetType = V4L2_ENC_HW_PRESET_FAST;
        break;
    case CompressionQuality::Fast:
        hwPresetType = V4L2_ENC_HW_PRESET_FAST;
        break;
    case CompressionQuality::Normal:
        hwPresetType = V4L2_ENC_HW_PRESET_MEDIUM;
        break;
    case CompressionQuality::Slow:
        hwPresetType = V4L2_ENC_HW_PRESET_SLOW;
        break;
    case CompressionQuality::Slower:
        hwPresetType = V4L2_ENC_HW_PRESET_SLOW;
        break;
    case CompressionQuality::Veryslow:
        hwPresetType = V4L2_ENC_HW_PRESET_SLOW;
        break;
    }
    if (encoder_->enc->setHWPresetType(hwPresetType) < 0) {
        LOGGER.error("Failed to setProfile");
        throw std::runtime_error("Failed to setProfile");
    }

    if (encoder_->enc->setProfile(V4L2_MPEG_VIDEO_H264_PROFILE_MAIN) < 0) {
        LOGGER.error("Failed to setProfile");
        throw std::runtime_error("Failed to setProfile");
    }

    if (encoder_->enc->setLevel(V4L2_MPEG_VIDEO_H264_LEVEL_5_0) < 0) {
        LOGGER.error("Failed to setLevel");
        throw std::runtime_error("Failed to setLevel");
    }

    if (encoder_->enc->setRateControlMode(V4L2_MPEG_VIDEO_BITRATE_MODE_VBR) < 0) {
        LOGGER.error("Failed to setRateControl");
        throw std::runtime_error("Failed to setRateControl");
    }

    if (encoder_->enc->setInsertSpsPpsAtIdrEnabled(true) < 0) {
        LOGGER.error("Failed to setInsertSpsPpsAtIdrEnabled");
        throw std::runtime_error("Failed to setInsertSpsPpsAtIdrEnabled");
    }

    if (encoder_->enc->setFrameRate(1, fps) < 0) {
        LOGGER.error("Failed to setFrameRate");
        throw std::runtime_error("Failed to setFrameRate");
    }

    if (encoder_->enc->setIFrameInterval(fps) < 0) {
        LOGGER.error("Failed to setIFrameInterval");
        throw std::runtime_error("Failed to setIFrameInterval");
    }

    if (encoder_->enc->setMaxPerfMode(1) < 0) {
        LOGGER.error("Failed to setMaxPerfMode");
        throw std::runtime_error("Failed to setMaxPerfMode");
    }

    if (encoder_->enc->setIDRInterval(2 * fps) < 0) {
        LOGGER.error("Failed to setIDRInterval");
        throw std::runtime_error("Failed to setIDRInterval");
    }

    if (encoder_->enc->output_plane.setupPlane(V4L2_MEMORY_USERPTR, 1, false, true) < 0) {
        LOGGER.error("Failed to setupPlane");
        throw std::runtime_error("Failed to setupPlane");
    }

    if (encoder_->enc->capture_plane.setupPlane(V4L2_MEMORY_MMAP, 1, true, false) < 0) {
        LOGGER.error("Failed to setupPlane");
        throw std::runtime_error("Failed to setupPlane");
    }

    if (encoder_->enc->output_plane.setStreamStatus(true) < 0) {
        LOGGER.error("Failed to setStreamStatus");
        throw std::runtime_error("Failed to setStreamStatus");
    }

    if (encoder_->enc->capture_plane.setStreamStatus(true) < 0) {
        LOGGER.error("Failed to setStreamStatus");
        throw std::runtime_error("Failed to setStreamStatus");
    }

    for (uint32_t i = 0; i < encoder_->enc->capture_plane.getNumBuffers(); i++) {
        struct v4l2_buffer v4l2_buf;
        struct v4l2_plane planes[MAX_PLANES];

        memset(&v4l2_buf, 0, sizeof(v4l2_buf));
        memset(planes, 0, MAX_PLANES * sizeof(struct v4l2_plane));

        v4l2_buf.index = i;
        v4l2_buf.m.planes = planes;
        if (encoder_->enc->capture_plane.qBuffer(v4l2_buf, NULL) < 0) {
            LOGGER.error("Failed to qBuffer");
            throw std::runtime_error("Failed to qBuffer");
        }
    }

    encoder_->enc->capture_plane.setDQThreadCallback(encoder_capture_plane_dq_callback);
    encoder_->enc->capture_plane.startDQThread(encoder_.get());
    encoder_->dqThreadStarted = true;
}

x264VideoEncoder::~x264VideoEncoder() {
    LOGGER.trace("DTor");
    if (encoder_->dqThreadStarted) {
        flush();
        encoder_->enc->capture_plane.waitForDQThread(2000);
    }

    encoder_->enc.reset(nullptr);
    encoder_.reset(nullptr);
}

bool x264VideoEncoder::addFrame(const VideoFrame& frame) {
    if (encoder_->flushed) {
        LOGGER.warn("Already flushed");
        return false;
    }

    auto converted = frame.get<VideoFrame>(resolution_.width, resolution_.height, "yuv420p");
    struct v4l2_buffer v4l2_buf;
    struct v4l2_plane planes[MAX_PLANES];

    NvBuffer* nvBuffer;

    memset(&v4l2_buf, 0, sizeof(v4l2_buf));
    memset(planes, 0, sizeof(planes));

    v4l2_buf.m.planes = planes;

    if (encoder_->enc->isInError()) {
        LOGGER.error("Encoder is in error");
        return false;
    }

    if (encoder_->index < encoder_->enc->output_plane.getNumBuffers()) {
        nvBuffer = encoder_->enc->output_plane.getNthBuffer(encoder_->index);
        v4l2_buf.index = encoder_->index;
        encoder_->index++;
    } else {
        if (encoder_->enc->output_plane.dqBuffer(v4l2_buf, &nvBuffer, NULL, -1) < 0) {
            LOGGER.error("Error while dequeueing buffer at output plane");
            return false;
        }
    }

    memcpy(nvBuffer->planes[0].data, converted.data(0), converted.stride(0) * converted.height());
    nvBuffer->planes[0].bytesused = converted.stride(0) * converted.height();
    memcpy(nvBuffer->planes[1].data, converted.data(1), converted.stride(1) * converted.height()/2);
    nvBuffer->planes[1].bytesused = converted.stride(1) * converted.height()/2;
    memcpy(nvBuffer->planes[2].data, converted.data(2), converted.stride(2) * converted.height()/2);
    nvBuffer->planes[2].bytesused = converted.stride(2) * converted.height()/2;

    v4l2_buf.flags |= V4L2_BUF_FLAG_TIMESTAMP_COPY;
    gettimeofday(&v4l2_buf.timestamp, NULL);

    if (encoder_->enc->output_plane.qBuffer(v4l2_buf, NULL) < 0) {
        LOGGER.error("Error while queueing buffer at output plane");
        return false;
    }

    return true;
}

std::vector<uint8_t> x264VideoEncoder::getBytes(bool clearBuffer) {
    std::scoped_lock lock(encoder_->bytesMtxQueue);
    if (flowBytes_.empty() && encoder_->bytes.empty()) {
        return std::vector<uint8_t>();
    }

    while (!encoder_->bytes.empty()) {
        auto bytes = encoder_->bytes.front();
        flowBytes_.insert(flowBytes_.end(), bytes.begin(), bytes.end());
        encoder_->bytes.pop();
    }

    if (clearBuffer) {
        auto copy = flowBytes_;
        flowBytes_.clear();
        return copy;
    }

    return flowBytes_;
}

bool x264VideoEncoder::flush() {
    LOGGER.trace("flush()");
    if (encoder_->flushed) {
        LOGGER.warn("Already flushed");
        return false;
    }

    struct v4l2_buffer v4l2_buf;
    struct v4l2_plane planes[MAX_PLANES];
    NvBuffer* nvBuffer;

    memset(&v4l2_buf, 0, sizeof(v4l2_buf));
    memset(planes, 0, sizeof(planes));

    v4l2_buf.m.planes = planes;

    if (encoder_->enc->isInError()) {
        LOGGER.error("Encoder is in error");
        return false;
    }

    if (encoder_->index < encoder_->enc->output_plane.getNumBuffers()) {
        nvBuffer = encoder_->enc->output_plane.getNthBuffer(encoder_->index);
        v4l2_buf.index = encoder_->index;
        encoder_->index++;
    } else {
        if (encoder_->enc->output_plane.dqBuffer(v4l2_buf, &nvBuffer, NULL, -1) < 0) {
            LOGGER.error("Error while dequeueing buffer at output plane");
            return false;
        }
    }

    nvBuffer->planes[0].bytesused = 0;

    if (encoder_->enc->output_plane.qBuffer(v4l2_buf, NULL) < 0) {
        LOGGER.error("Error while queueing buffer at output plane");
        return false;
    }

    encoder_->flushed = true;

    return true;
}

CompressionQuality x264VideoEncoder::getCompressionQuality() {
    LOGGER.trace("getCompressionQuality");
    return quality_;
}

VideoResolution x264VideoEncoder::getResolution() {
    LOGGER.trace("getResolution");
    return resolution_;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
