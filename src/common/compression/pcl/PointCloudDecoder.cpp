#include "urf/algorithms/compression/pcl/PointCloudDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

PointCloudDecoder::PointCloudDecoder(bool octreeCompression, std::unique_ptr<IDataDecoder> decoder)
    : octreeCompression_(octreeCompression)
    , decoder_(std::move(decoder)) { }

bool PointCloudDecoder::addBytes(const std::vector<uint8_t>& bytes) {
    pointTypes_.push_back(bytes[0]);
    if (decoder_) {
        decoder_->addBytes(std::vector<uint8_t>(bytes.begin() + 1, bytes.end()));
    } else {
        buffer_.insert(buffer_.end(), bytes.begin() + 1, bytes.end());
    }

    return true;
}

template <>
uint8_t PointCloudDecoder::getPointType<pcl::PointXYZ>() {
    return 0;
}

template <>
uint8_t PointCloudDecoder::getPointType<pcl::PointXYZRGB>() {
    return 1;
}

template <>
uint8_t PointCloudDecoder::getPointType<pcl::PointXYZRGBA>() {
    return 2;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
