#include "urf/algorithms/compression/pcl/PointCloudEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

PointCloudEncoder::PointCloudEncoder(bool octreeCompression, std::unique_ptr<IDataEncoder> encoder)
    : octreeCompression_(octreeCompression)
    , compressionLevel_(pcl::io::MED_RES_ONLINE_COMPRESSION_WITH_COLOR)
    , encoder_() {
    if (encoder != nullptr) {
        encoder_ = std::move(encoder);
    }
}

template <>
uint8_t PointCloudEncoder::getPointType<pcl::PointXYZ>() {
    return 0;
}

template <>
uint8_t PointCloudEncoder::getPointType<pcl::PointXYZRGB>() {
    return 1;
}

template <>
uint8_t PointCloudEncoder::getPointType<pcl::PointXYZRGBA>() {
    return 2;
}

bool PointCloudEncoder::setCompressionLevel(CompressionQuality level) {
    if (!octreeCompression_) {
        return false;
    }

    if (level == CompressionQuality::Low) {
        compressionLevel_ = pcl::io::LOW_RES_ONLINE_COMPRESSION_WITH_COLOR;
    } else if (level == CompressionQuality::Medium) {
        compressionLevel_ = pcl::io::MED_RES_ONLINE_COMPRESSION_WITH_COLOR;
    } else if (level == CompressionQuality::High) {
        compressionLevel_ = pcl::io::HIGH_RES_ONLINE_COMPRESSION_WITH_COLOR;
    }
    return true;
}

std::string PointCloudEncoder::getName() {
    std::string encoder = encoder_ ? "pcl-" + encoder_->getName() : "pcl";
    encoder = octreeCompression_ ? encoder + "-oc" : encoder;
    return encoder;
}

} // namespace compression
} // namespace algorithms
} // namespace urf
