#include <stdexcept>

#include "urf/algorithms/compression/VideoCompressionFactory.hpp"

#include "common/compression/jpeg/JpegVideoDecoder.hpp"
#include "common/compression/jpeg/JpegVideoEncoder.hpp"

#include "common/compression/x264/x264VideoDecoder.hpp"
#include "common/compression/x264/x264VideoEncoder.hpp"

#include "common/compression/x265/x265VideoDecoder.hpp"
#include "common/compression/x265/x265VideoEncoder.hpp"

#include "common/compression/bmp/BitmapDecoder.hpp"
#include "common/compression/bmp/BitmapEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

std::unique_ptr<IVideoDecoder> VideoCompressionFactory::getDecoder(const std::string& format) {
    if (format == "jpeg") {
        return std::move(std::make_unique<JpegVideoDecoder>());
    } else if (format == "x264") {
        return std::move(std::make_unique<x264VideoDecoder>());
    } else if (format == "x265") {
        return std::move(std::make_unique<x265VideoDecoder>());
    } else if (format == "bmp") {
        return std::move(std::make_unique<BitmapDecoder>());
    }

    throw std::invalid_argument("Invalid decoder requested. Available decoders: jpeg, x264");
}

std::unique_ptr<IVideoEncoder> VideoCompressionFactory::getEncoder(const std::string& format,
                                                                   const VideoResolution& resolution,
                                                                   uint16_t fps,
                                                                   CompressionQuality quality,
                                                                   bool zeroLatency) {
    if (format == "jpeg") {
        return std::move(std::make_unique<JpegVideoEncoder>(resolution, quality));
    } else if (format == "bmp") {
        return std::move(std::make_unique<BitmapEncoder>(resolution));
    } else if (format == "x264") {
        return std::move(std::make_unique<x264VideoEncoder>(resolution, fps, quality, zeroLatency));
    } else if (format == "x265") {
        return std::move(std::make_unique<x265VideoEncoder>(resolution, fps, quality, zeroLatency));
    }

    throw std::invalid_argument("Invalid encoder requested. Available decoders: jpeg, x264");
}

} // namespace compression
} // namespace algorithms
} // namespace urf
