#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/algorithms/urf_algorithms_export.h"
#else
#    define URF_ALGORITHMS_EXPORT
#endif

#include <memory>
#include <iostream>
#include <string>

namespace urf {
namespace algorithms {
namespace compression {

struct VideoResolution {
   uint32_t width;
   uint32_t height;

   VideoResolution();
   VideoResolution(uint32_t w, uint32_t h);

   bool empty() const;
};

class URF_ALGORITHMS_EXPORT VideoFrame {
 public:
    VideoFrame();
    VideoFrame(uint32_t width, uint32_t height, const std::string& format);
    VideoFrame(const VideoFrame& frame);
    VideoFrame(VideoFrame&& frame);
    template <class T>
    explicit VideoFrame(T* frame);

    ~VideoFrame();

    uint32_t width() const;
    uint32_t height() const;
    uint32_t stride(uint32_t plane) const;
    uint32_t planes() const;

    bool empty() const;

    std::string pixelFormat() const;

    const uint8_t* data(uint32_t plane) const;

    template <class T>
    T get(uint32_t width, uint32_t height, const std::string& format = "") const;

    const void* ptr() const;

    VideoFrame& operator=(VideoFrame&& rhs);
    VideoFrame& operator=(const VideoFrame& rhs);

    template<class T>
    static VideoFrame from(const T& frame, uint32_t width = 0, uint32_t height = 0, const std::string& format = "");

 private:
    struct frame_wrapper;
    std::unique_ptr<frame_wrapper> _frame;
};

std::ostream& operator<<(std::ostream& os, const VideoResolution& res);
bool operator==(const VideoResolution& lhs, const VideoResolution& rhs);

} // namespace compression
} // namespace algorithms
} // namespace urf
