#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/algorithms/urf_algorithms_export.h"
#else
    #define URF_ALGORITHMS_EXPORT
#endif

#include "urf/algorithms/compression/IDataEncoder.hpp"

#include <lz4.h>

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT LZ4DataEncoder : public IDataEncoder {
 public:
    LZ4DataEncoder(bool stream);
    ~LZ4DataEncoder() override;

    std::vector<uint8_t> encode(const std::vector<uint8_t>&) override;
    std::vector<uint8_t> encode(const uint8_t*, size_t) override;

    std::string getName() { return "lz4"; }

 private:
    template <typename T>
    inline void pack(std::vector<uint8_t>& dst, T data) const {
        uint8_t* src = reinterpret_cast<uint8_t*>(&data);
        dst.insert(dst.end(), src, src + sizeof(T));
    }

 private:
    const uint32_t blockSize_ = 65535;
    LZ4_stream_t* stream_;
    size_t compressBound_;
    char* outBuffer_;
    bool useStream_;
};

}  // namespace compression
}  // namespace algorithms
}  // namespace urf