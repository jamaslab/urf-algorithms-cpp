#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/algorithms/urf_algorithms_export.h"
#else
    #define URF_ALGORITHMS_EXPORT
#endif

#include <cstring>

#include <lz4.h>

#include <urf/algorithms/compression/IDataDecoder.hpp>

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT LZ4DataDecoder : public IDataDecoder {
 public:
    LZ4DataDecoder(bool stream);
    ~LZ4DataDecoder() override;

    bool addBytes(const std::vector<uint8_t>&) override;
    std::vector<uint8_t> decode(bool clearBuffer) override;

    std::string getName() override { return "lz4"; }
 private:
    template <typename T>
    inline void unpack(const std::vector<uint8_t>& src, int index, T& data) {
        std::memcpy(&data, src.data()+index, sizeof(T));
    }

 private:
    const uint32_t blockSize_ = 65535;
    LZ4_streamDecode_t* stream_;
    size_t compressBound_;
    char* outBuffer_;

    std::vector<uint8_t> buffer_;
    bool useStream_;
};

}  // namespace compression
}  // namespace algorithms
}  // namespace urf