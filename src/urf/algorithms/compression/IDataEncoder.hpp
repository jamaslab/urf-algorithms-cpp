#pragma once

#if defined(_WIN32) || defined(_WIN64)
#	include "urf/algorithms/urf_algorithms_export.h"
#else
#	define URF_ALGORITHMS_EXPORT
#endif

#include <cstdint>
#include <string>
#include <vector>

namespace urf {
namespace algorithms {
namespace compression {

/**
 * @brief IDataEncoder interface
 * The IDataEncoder interface is meant to standardise bytes encoding methods
 * It allows to add std::vector of bytes and obtain encoded bytes
 */
class URF_ALGORITHMS_EXPORT IDataEncoder {
 public:
	virtual ~IDataEncoder() = default;

	/**
     * @brief Encodes a chunk of bytes to the encoder
     * @return the encoded bytes
     * @return empty vector if the encoding failed
     */
	virtual std::vector<uint8_t> encode(const std::vector<uint8_t>&) = 0;

	/**
     * @brief Encodes a chunk of bytes to the encoder
     * @return the encoded bytes
     * @return empty vector if the encoding failed
     */
	virtual std::vector<uint8_t> encode(const uint8_t*, size_t) = 0;

	/**
     * @brief Get the encoding name
     * @return std::string encoding name
     */
	virtual std::string getName() = 0;
};

} // namespace compression
} // namespace algorithms
} // namespace urf