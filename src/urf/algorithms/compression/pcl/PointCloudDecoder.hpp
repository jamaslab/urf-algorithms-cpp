#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/algorithms/urf_algorithms_export.h"
#else
    #define URF_ALGORITHMS_EXPORT
#endif

#include <cstdint>
#include <streambuf>
#include <string>
#include <vector>

#include <pcl/pcl_base.h>
#include <pcl/point_types.h>
#include <pcl/compression/octree_pointcloud_compression.h>

#include "urf/algorithms/compression/IDataDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT PointCloudDecoder {
 public:
    PointCloudDecoder(bool octreeCompression, std::unique_ptr<IDataDecoder> decoder = nullptr);
    ~PointCloudDecoder() = default;
    
    bool addBytes(const std::vector<uint8_t>&);
    template<typename PointT>
    typename pcl::PointCloud<PointT>::Ptr decode(bool clearBuffer = true);

    std::string getName() { return decoder_ ? "pcl-"+decoder_->getName() : "pcl"; }

 private:
    bool octreeCompression_;
    std::unique_ptr<IDataDecoder> decoder_;
    std::vector<uint8_t> pointTypes_;
    std::vector<uint8_t> buffer_;

    template<class PointT>
    uint8_t getPointType();

    template<typename CharT, typename TraitsT = std::char_traits<CharT> >
    class vectorwrapbuf : public std::basic_streambuf<CharT, TraitsT> {
    public:
        vectorwrapbuf(std::vector<CharT> &vec) {
            this->setg(vec.data(), vec.data(), vec.data() + vec.size());
        }

        vectorwrapbuf(CharT* vec, uint32_t size) {
            this->setg(vec, vec, vec + size);
        }
    };
};

template<typename PointT>
typename pcl::PointCloud<PointT>::Ptr PointCloudDecoder::decode(bool clearBuffer) {
    if (pointTypes_[0] != getPointType<PointT>()) {
        return nullptr;
    }

    std::vector<uint8_t> back_buffer;
    typename pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
    uint8_t* dataPtr = nullptr;
    uint32_t dataSize = 0;
    if (decoder_) {
        back_buffer = decoder_->decode(clearBuffer);

        if (back_buffer.size() == 0) {
            return nullptr;
        }

        size_t numberOfPoints = back_buffer.size() / sizeof(PointT);

        cloud->resize(numberOfPoints);
        std::memcpy(&(cloud->points[0]), back_buffer.data(), back_buffer.size());

        dataPtr = back_buffer.data();
        dataSize = back_buffer.size();
    } else {
        std::memcpy(&dataSize, buffer_.data(), sizeof(dataSize));
        if (buffer_.size() < dataSize) {
            return nullptr;
        }

        dataPtr =  buffer_.data() + sizeof(dataSize);   
    }

    if (octreeCompression_) {
        auto pclDecoder = std::make_unique<pcl::io::OctreePointCloudCompression<PointT>>();
        vectorwrapbuf<char> databuf(reinterpret_cast<char*>(dataPtr), dataSize);
        std::istream is(&databuf);
        pclDecoder->decodePointCloud(is, cloud);
    } else {
        size_t numberOfPoints = dataSize / sizeof(PointT);
        cloud->resize(numberOfPoints);
        std::memcpy(&(cloud->points[0]), dataPtr, dataSize);
    }

    if (clearBuffer) {
        pointTypes_.clear();
    }

    return cloud;
}

}  // namespace compression
}  // namespace algorithms
}  // namespace urf