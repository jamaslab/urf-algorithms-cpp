#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/algorithms/urf_algorithms_export.h"
#else
    #define URF_ALGORITHMS_EXPORT
#endif

#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <pcl/pcl_base.h>
#include <pcl/point_types.h>
#include <pcl/compression/octree_pointcloud_compression.h>

#include "urf/algorithms/compression/IDataEncoder.hpp"


namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT PointCloudEncoder {
 public:
    enum class CompressionQuality { Low, Medium, High };

    PointCloudEncoder(bool octreeCompression, std::unique_ptr<IDataEncoder> encoder = nullptr);
    ~PointCloudEncoder() = default;
    template<typename PointT>
    std::vector<uint8_t> encode(typename pcl::PointCloud<PointT>::Ptr cloud);

    bool setCompressionLevel(CompressionQuality level);

    std::string getName();

 private:
    bool octreeCompression_;
    pcl::io::compression_Profiles_e compressionLevel_;
    std::unique_ptr<IDataEncoder> encoder_;

    template<class PointT>
    uint8_t getPointType();
};

template<typename PointT>
std::vector<uint8_t> PointCloudEncoder::encode(typename pcl::PointCloud<PointT>::Ptr cloud) {
    auto pointSize = sizeof(PointT);
    auto pointType = getPointType<PointT>();

    uint8_t* dataPtr = reinterpret_cast<uint8_t*>(&(cloud->points[0]));
    uint32_t dataSize = pointSize*cloud->size();
    std::string str;
    std::vector<uint8_t> outBuffer({pointType});

    if (octreeCompression_) {
        auto octreeEncoder = std::make_unique<pcl::io::OctreePointCloudCompression<PointT>>(compressionLevel_);
        std::stringstream compressedData;
        octreeEncoder->encodePointCloud(cloud, compressedData);
        str = compressedData.str();
        dataPtr = (uint8_t*)(str.c_str());
        dataSize = str.size();
    }    

    if (encoder_) {
        auto encoded = encoder_->encode(dataPtr, dataSize);
        outBuffer.insert(outBuffer.end(), encoded.begin(), encoded.end());
    } else {
        outBuffer.insert(outBuffer.end(), reinterpret_cast<uint8_t*>(&dataSize), reinterpret_cast<uint8_t*>(&dataSize)+sizeof(dataSize));
        outBuffer.insert(outBuffer.end(), dataPtr, dataPtr+dataSize);
    }

    return outBuffer;
}

}  // namespace compression
}  // namespace algorithms
}  // namespace urf
