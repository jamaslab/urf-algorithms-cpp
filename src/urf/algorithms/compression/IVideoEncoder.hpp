#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/algorithms/urf_algorithms_export.h"
#else
#    define URF_ALGORITHMS_EXPORT
#endif

#include <vector>
#include "urf/algorithms/compression/VideoFrame.hpp"

namespace urf {
namespace algorithms {
namespace compression {

enum class URF_ALGORITHMS_EXPORT CompressionQuality {
    Ultrafast = 0,
    Superfast = 1,
    Veryfast = 2,
    Faster = 3,
    Fast = 4,
    Normal = 5,
    Slow = 6,
    Slower = 7,
    Veryslow = 8
};

/**
 * @brief VideoEncoder interface
 * The VideoEncoder interface is meant to standardise video encoding methods
 * It allows to add cv::Mat frames and obtain encoded bytes
 */
class URF_ALGORITHMS_EXPORT IVideoEncoder {
 public:
    virtual ~IVideoEncoder() = default;

    /**
     * @brief Add a frame to the encoder
     * @return true the frame was correctly added
     * @return false otherwise
     */
    virtual bool addFrame(const VideoFrame&) = 0;

    /**
     * @brief Get the compressed bytes
     * @param clearBuffer the buffer compressed bytes is cleared
     * @return std::vector<uint8_t> the compressed bytes
     */
    virtual std::vector<uint8_t> getBytes(bool clearBuffer = true) = 0;

    /**
     * @brief Flushes the video encoder algorithm
     * After flush, it is not anymore possible to add frames.
     * @return true flush was successful
     * @return false otherwise
     */
    virtual bool flush() = 0;

    /**
     * @brief Get the Compression Quality
     * @return CompressionQuality
     */
    virtual CompressionQuality getCompressionQuality() = 0;

    /**
     * @brief Get the encoding resolution
     * @return VideoResolution encoding resolution
     */
    virtual VideoResolution getResolution() = 0;

    /**
     * @brief Get the encoding name
     * @return std::string encoding name
     */
    virtual std::string getName() = 0;
};

} // namespace compression
} // namespace algorithms
} // namespace urf