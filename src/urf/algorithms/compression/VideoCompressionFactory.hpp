#pragma once

#if defined(_WIN32) || defined(_WIN64)
#	include "urf/algorithms/urf_algorithms_export.h"
#else
#	define URF_ALGORITHMS_EXPORT
#endif

#include <memory>
#include <string>

#include "urf/algorithms/compression/IVideoDecoder.hpp"
#include "urf/algorithms/compression/IVideoEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT VideoCompressionFactory {
 public:
	static std::unique_ptr<IVideoDecoder> getDecoder(const std::string&);
	static std::unique_ptr<IVideoEncoder>
	getEncoder(const std::string&, const VideoResolution&, uint16_t fps, CompressionQuality, bool lowLatency);

 private:
	VideoCompressionFactory() = default;
};

} // namespace compression
} // namespace algorithms
} // namespace urf
