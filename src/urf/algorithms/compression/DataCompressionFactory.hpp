#pragma once

#if defined(_WIN32) || defined(_WIN64)
#	include "urf/algorithms/urf_algorithms_export.h"
#else
#	define URF_ALGORITHMS_EXPORT
#endif

#include <memory>
#include <string>

#include "urf/algorithms/compression/IDataDecoder.hpp"
#include "urf/algorithms/compression/IDataEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT DataCompressionFactory {
 public:
	static std::unique_ptr<IDataDecoder> getDecoder(const std::string&, bool stream);
	static std::unique_ptr<IDataEncoder> getEncoder(const std::string&, bool stream);

 private:
	DataCompressionFactory() = default;
};

} // namespace compression
} // namespace algorithms
} // namespace urf
