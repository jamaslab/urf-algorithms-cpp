#pragma once

#if defined(_WIN32) || defined(_WIN64)
#	include "urf/algorithms/urf_algorithms_export.h"
#else
#	define URF_ALGORITHMS_EXPORT
#endif

#include <vector>

#include "urf/algorithms/compression/VideoFrame.hpp"

namespace urf {
namespace algorithms {
namespace compression {

/**
 * @brief IVideoDecoder interface
 * The IVideoDecoder interface is meant to standardise video decoding methods
 * It allows to add encoded bytes and to obtain cv::Mat frames
 */
class URF_ALGORITHMS_EXPORT IVideoDecoder {
 public:
	virtual ~IVideoDecoder() = default;
	/**
     * @brief Add bytes to the decoder
     * @param bytes bytes to add
     * @return true bytes were correctly added
     * @return false otherwise
     */
	virtual bool addBytes(const std::vector<uint8_t>& bytes) = 0;

	/**
     * @brief Get a frame from the decoder
     * It returns an empty cv::Mat if the stored bytes did not contain a valid frame
     * @return cv::Mat the decoded frame
     */
	virtual VideoFrame getFrame() = 0;

	/**
     * @brief Clears the current buffer
     *
     * @return true if the buffer was correctly cleared
     * @return false otherwise
     */
	virtual bool clear() = 0;
};

} // namespace compression
} // namespace algorithms
} // namespace urf
