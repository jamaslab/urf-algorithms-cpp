#pragma once

#if defined(_WIN32) || defined(_WIN64)
#	include "urf/algorithms/urf_algorithms_export.h"
#else
#	define URF_ALGORITHMS_EXPORT
#endif

#include <cstdint>
#include <string>
#include <vector>

namespace urf {
namespace algorithms {
namespace compression {

/**
 * @brief IDataDecoder interface
 * The IDataDecoder interface is meant to standardise bytes decoding methods
 * It allows to add std::vector of bytes and obtain decpded bytes
 */
class URF_ALGORITHMS_EXPORT IDataDecoder {
 public:
	virtual ~IDataDecoder() = default;

	/**
     * @brief Add a chunk of bytes to the decoder
     * @return true the bytes was correctly added
     * @return false otherwise
     */
	virtual bool addBytes(const std::vector<uint8_t>&) = 0;

	/**
     * @brief Decodes the added bytes
     * @param clearBuffer the encoded bytes buffer is cleared
     * @return std::vector<uint8_t> the decoded bytes
     */
	virtual std::vector<uint8_t> decode(bool clearBuffer = true) = 0;

	/**
     * @brief Get the encoding name
     * @return std::string encoding name
     */
	virtual std::string getName() = 0;
};

} // namespace compression
} // namespace algorithms
} // namespace urf