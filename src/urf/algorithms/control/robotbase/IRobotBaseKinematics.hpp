#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/algorithms/urf_algorithms_export.h"
#else
#    define URF_ALGORITHMS_EXPORT
#endif

#include <vector>

namespace urf {
namespace algorithms {
namespace control {

/**
 * @brief IRobotBaseKinematics interface
 * The IRobotBaseKinematics interface is meant to standardise robot base kinematics.
 * It converts cartesian velocity in the x-y plane into wheels velocity and vice-versa
 */
class URF_ALGORITHMS_EXPORT IRobotBaseKinematics {
 public:
    virtual ~IRobotBaseKinematics() = default;

    /**
     * @brief Calculates wheels velocity from inputed cartesian velocity
     * @param v_x robot base velocity in m/s on the x axis
     * @param v_y robot base velocity in m/s on the y axis
     * @param w robot base angular velocity around the z axis
     * @return the wheels velocity in rad/s
     * @throws exception if calculation failed
     */
    virtual std::vector<float> getWheelsVelocity(float v_x, float v_y, float w) const = 0;

    /**
     * @brief Calculates cartesian velocity from inputed wheels velocity
     * @param wheelsVelocity a vector containing the velocity of each wheel in m/s
     * @return a vector containing the cartesian velocity in m/s
     * @throws invalid_argument if input vector is the wrong size
     * @throws exception if calculation failed
     */
    virtual std::vector<float> getCartesianVelocity(const std::vector<float>& wheelsVelocity) const = 0;
};

} // namespace control
} // namespace algorithms
} // namespace urf