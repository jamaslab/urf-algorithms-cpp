#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/algorithms/urf_algorithms_export.h"
#else
#    define URF_ALGORITHMS_EXPORT
#endif

#include "urf/algorithms/control/robotbase/IRobotBaseKinematics.hpp"

namespace urf {
namespace algorithms {
namespace control {

class URF_ALGORITHMS_EXPORT RobotBaseOmnidirectionalKinematics : public IRobotBaseKinematics {
 public:
    RobotBaseOmnidirectionalKinematics() = delete;
    RobotBaseOmnidirectionalKinematics(float wheelRadius, float wheelCenterX, float wheelCenterY);
    RobotBaseOmnidirectionalKinematics(const RobotBaseOmnidirectionalKinematics&) = default;
    RobotBaseOmnidirectionalKinematics(RobotBaseOmnidirectionalKinematics&&) = default;
    ~RobotBaseOmnidirectionalKinematics() override = default;

    std::vector<float> getWheelsVelocity(float v_x, float v_y, float w) const override final;
    std::vector<float> getCartesianVelocity(const std::vector<float>& wheelsVelocity) const override final;

 private:
    float wheelRadius_;
    float wheelCenterX_;
    float wheelCenterY_;
};

} // namespace control
} // namespace algorithms
} // namespace urf