#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/algorithms/urf_algorithms_export.h"
#else
#    define URF_ALGORITHMS_EXPORT
#endif

#include <Eigen/Core>

namespace urf {
namespace algorithms {
namespace filters {

class URF_ALGORITHMS_EXPORT MadgwickFilter {
 public:
    MadgwickFilter() = delete;
    MadgwickFilter(float frequency, float beta);

    bool reset();
    bool update(const Eigen::Vector3f& accelerations, const Eigen::Vector3f& gyroscopes);
    bool update(const Eigen::Vector3f& accelerations, const Eigen::Vector3f& gyroscopes, const Eigen::Vector3f& magnetometers);

    Eigen::Vector4f quaternions() const;
    Eigen::Vector3f angles() const;

 private:
    float _beta;
    Eigen::Vector4f _q;
    float _freq;
    float _invFreq;
};

} // namespace filters
} // namespace algorithms
} // namespace urf