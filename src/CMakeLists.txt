set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_FIND_FRAMEWORK LAST)

find_package(urf_common_cpp REQUIRED)

set(sources)
set(public_libs
    ${urf_common_cpp_LIBRARIES})

set(private_libs)

set(include_dirs
    ${urf_common_cpp_INCLUDE_DIRS})
set(install_headers)

if (VIDEO_COMPRESSION)
    find_package(OpenCV REQUIRED)
    find_package(ffmpeg REQUIRED)
    find_package(libx264 REQUIRED)
    find_package(libx265 REQUIRED)

    set(sources
        ${sources}
        common/compression/VideoFrame.cpp
        common/compression/VideoCompressionFactory.cpp
        common/compression/bmp/BitmapEncoder.cpp
        common/compression/bmp/BitmapDecoder.cpp
    )

    set(private_libs
        ${private_libs}
        ${opencv_LIBRARIES}
        ${ffmpeg_LIBRARIES}
        ${libx264_LIBRARIES}
        ${libx265_LIBRARIES}
    )

    set(include_dirs
        ${include_dirs}
        ${opencv_INCLUDE_DIRS}
        ${ffmpeg_INCLUDE_DIRS}
        ${libx264_INCLUDE_DIRS}
        ${libx265_INCLUDE_DIRS}
    )

    set(install_headers
        ${install_headers}
        urf/algorithms/compression/VideoFrame.hpp
        urf/algorithms/compression/IVideoDecoder.hpp
        urf/algorithms/compression/IVideoEncoder.hpp
        urf/algorithms/compression/VideoCompressionFactory.hpp
    )

    if(JETSON)
        find_package(TegraMM REQUIRED)
        ## ADding these two requirements explicitly, apparently the don't get linked automatically
        find_package(Iconv REQUIRED)
        find_package(LibLZMA REQUIRED)
        
        if(${TegraMM_FOUND})
            message(STATUS "Found Tegra Multimedia API")
        else()
            message(STATUS "Could not find Tegra Multimedia API")
        endif()

        set(private_libs
            ${private_libs}
            ${Iconv_LIBRARIES}
            ${LibLZMA_LIBRARIES}
            ${TegraMM_LIBRARIES}
            ${opencv_LIB_DIRS}/libtegra_hal.a
        )

        set(include_dirs
            ${include_dirs}
            ${TegraMM_INCLUDE_DIRS}
        )

        set(sources
            ${sources}
            ${TegraMM_COMMON_SOURCES}
            common/compression/jetson/JpegVideoEncoder.cpp
            common/compression/jetson/JpegVideoDecoder.cpp
            common/compression/jetson/nvmpi/nvmpi_dec.cpp
            common/compression/jetson/x264VideoEncoder.cpp
            common/compression/jetson/x264VideoDecoder.cpp
            common/compression/x265/x265VideoEncoder.cpp
            common/compression/x265/x265VideoDecoder.cpp
        )
    else()
        set(sources
            ${sources}
            common/compression/jpeg/JpegVideoDecoder.cpp
            common/compression/jpeg/JpegVideoEncoder.cpp
            common/compression/x264/x264VideoDecoder.cpp
            common/compression/x264/x264VideoEncoder.cpp
            common/compression/x265/x265VideoEncoder.cpp
            common/compression/x265/x265VideoDecoder.cpp
        )
    endif()
endif(VIDEO_COMPRESSION)

if (POINTCLOUD_COMPRESSION)
    find_package(PCL REQUIRED)
    set(sources
        ${sources}
        common/compression/pcl/PointCloudEncoder.cpp
        common/compression/pcl/PointCloudDecoder.cpp)

    set(DATA_COMPRESSION ON)

    set(public_libs
        ${public_libs}
        ${PCL_LIBRARIES}
    )

    set(include_dirs
        ${include_dirs}
        ${PCL_INCLUDE_DIRS}
    )

    set(install_headers
        ${install_headers}
        urf/algorithms/compression/pcl/PointCloudDecoder.hpp
        urf/algorithms/compression/pcl/PointCloudEncoder.hpp
    )
endif(POINTCLOUD_COMPRESSION)

if (DATA_COMPRESSION)
    find_package(lz4 REQUIRED)

    set(sources
        ${sources}
        common/compression/DataCompressionFactory.cpp
        common/compression/lz4/LZ4DataEncoder.cpp
        common/compression/lz4/LZ4DataDecoder.cpp
    )

    set(public_libs
        ${public_libs}
        ${lz4_LIBRARIES}
    )

    set(include_dirs
        ${include_dirs}
        ${lz4_INCLUDE_DIRS}
    )

    set(install_headers
        ${install_headers}
        urf/algorithms/compression/lz4/LZ4DataEncoder.hpp
        urf/algorithms/compression/lz4/LZ4DataDecoder.hpp
        urf/algorithms/compression/DataCompressionFactory.hpp
        urf/algorithms/compression/IDataDecoder.hpp
        urf/algorithms/compression/IDataEncoder.hpp
    )
endif(DATA_COMPRESSION)

if (ROBOT_CONTROL)

    set(sources
        ${sources}
        common/control/robotbase/RobotBaseOmnidirectionalKinematics.cpp
    )

    set(install_headers
        ${install_headers}
        urf/algorithms/control/robotbase/IRobotBaseKinematics.hpp
        urf/algorithms/control/robotbase/RobotBaseOmnidirectionalKinematics.hpp
    )
endif(ROBOT_CONTROL)

if (FILTERS)
    find_package(Eigen3 REQUIRED)

    set(sources
        ${sources}
        common/filters/imu/MadgwickFilter.cpp
    )

    set(public_libs
        ${public_libs}
        ${Eigen3_LIBRARIES}
    )

    set(include_dirs
        ${include_dirs}
        ${Eigen3_INCLUDE_DIRS}
    )

    set(install_headers
        ${install_headers}
        urf/algorithms/filters/imu/MadgwickFilter.hpp
    )
endif(FILTERS)

if (WIN32)

set (public_libs
    ${public_libs}
    wsock32)

endif(WIN32)

if (JETSON)
    list(REMOVE_ITEM private_libs JPEG::JPEG)
    list(REMOVE_ITEM public_libs JPEG::JPEG)
endif()

add_library(urf_algorithms ${sources})
if (BUILD_SHARED_LIBS)
    target_link_libraries(urf_algorithms PRIVATE ${private_libs})
else()
    target_link_libraries(urf_algorithms PUBLIC ${private_libs})
endif()

target_link_libraries(urf_algorithms PUBLIC ${public_libs})

target_include_directories(urf_algorithms SYSTEM PUBLIC ${include_dirs})
target_include_directories(urf_algorithms PUBLIC ${PROJECT_SOURCE_DIR}/src/)

if (WIN32)
    include(GenerateExportHeader)
    set(EXPORT_HEADER_PATH ${CMAKE_CURRENT_BINARY_DIR}/urf/algorithms/urf_algorithms_export.h)
    generate_export_header(urf_algorithms EXPORT_FILE_NAME ${EXPORT_HEADER_PATH})
    install(FILES "${EXPORT_HEADER_PATH}" DESTINATION include/urf/algorithms)
endif(WIN32)

install(TARGETS urf_algorithms EXPORT urf_algorithms DESTINATION lib/)

foreach (file ${install_headers})
    get_filename_component(dir ${file} DIRECTORY)
    install(FILES ${file} DESTINATION include/${dir})
endforeach()

add_subdirectory(bin)