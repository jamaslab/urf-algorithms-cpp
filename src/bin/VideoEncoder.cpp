#include <chrono>
#include <iostream>
#include <thread>

#include <opencv2/core.hpp>
// #include <opencv2/dnn_superres.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <urf/algorithms/compression/VideoCompressionFactory.hpp>

using namespace urf::algorithms::compression;

int main(int argc, char* argv[]) {
    if (argc != 7) {
        std::cout << "Invalid inputs" << std::endl;
        return -1;
    }

    std::string videoFilename(argv[1]);
    std::string format(argv[2]);
    CompressionQuality quality = (CompressionQuality)std::stoi(argv[5]);
    int fps = std::stoi(argv[6]);
    auto targetResolution = VideoResolution(std::stoi(argv[3]), std::stoi(argv[4]));

    auto encoder =
        VideoCompressionFactory::getEncoder(format, targetResolution, fps, quality, true);
    auto decoder = VideoCompressionFactory::getDecoder(format);

    std::cout << "Setting up everything" << std::endl;
    std::cout << "Filename: " << videoFilename << std::endl;
    std::cout << "Resolution: " << targetResolution << std::endl;
    std::cout << "Framerate: " << fps << std::endl;
    std::cout << "Encoding: " << format << std::endl;

    auto capture = cv::VideoCapture(videoFilename);
    auto lastFrameTime = std::chrono::system_clock::now();

    // cv::dnn_superres::DnnSuperResImpl sr;
    // sr.readModel("E:\\workspace\\data\\models\\sr\\ESPCN_x4.pb");
    // sr.setModel("espcn", 4);

    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(30));

        cv::Mat frame;
        capture >> frame;

        if (frame.empty()) {
            break;
        }

        cv::resize(frame, frame, cv::Size(targetResolution.width, targetResolution.height));
        auto now = std::chrono::system_clock::now();
        auto lastFrameMs = static_cast<float>(
            std::chrono::duration_cast<std::chrono::milliseconds>(now - lastFrameTime).count());
        if (1000.0f / lastFrameMs > fps)
            continue;

        lastFrameTime = now;

        encoder->addFrame(VideoFrame::from(frame));

        auto bytes = encoder->getBytes();

        std::cout << bytes.size() << std::endl;

        decoder->addBytes(bytes);
        auto backFrame = decoder->getFrame();

        if (!frame.empty()) {
            cv::imshow("back frame", backFrame.get<cv::Mat>(backFrame.width(), backFrame.height()));

            // cv::Mat upscaled;
            // sr.upsample(backFrame, upscaled);
            // cv::imshow("upscaled", upscaled);

            if (cv::waitKey(30) >= 0)
                break;
        }
    }
    return 0;
}